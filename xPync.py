#!/usr/bin/env python

####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

import argparse

from cmd import *


# Clear before write
def _add_clear(parser):
    parser.add_argument('--clear', action='store_true', help='clear existing data before upload')


# Common arguments
def _add_common(parser):
    parser.add_argument('--log-console', action='store_true', help='direct log output to console')
#    parser.add_argument('--log-file', help='file for log output')
    parser.add_argument('--log-level', choices=['DEBUG', 'INFO', 'WARNING', 'ERROR'], default='INFO', help='log level')
    parser.add_argument('--log-xnat', action='store_true', help='add XNATPy logging')


# custom variable + assessor arguments
def _add_cv_assessor(parser):
    parser.add_argument('--no-cv', action='store_true', help='ignore custom variables')
    parser.add_argument('--no-assessors', action='store_true', help='ignore all assessors, overrides --roi-only')
    parser.add_argument('--roi-only', action='store_true', help='ignore all assessors that are not ROI Collections')


# Host + user arguments
def _add_host_user(parser):
    parser.add_argument('host', nargs='?', default=None, help='host to connect to, may be an alias')
    parser.add_argument('user', nargs='?', default=None, help='user to connect as')


# Project
def _add_project(parser):
    parser.add_argument('--project', help='Process single project not entire server')


# Working directory
def _add_work_dir(parser):
    parser.add_argument('--work-dir', help='specifies working directory, defaults to "."')


####
def _copy(args):
    cmd = CmdCopy()
    cmd.run(args)


# copy command
def _copy_parser(subparsers):
    parser = subparsers.add_parser('copy', help='Copy data from source host to target host')
    parser.set_defaults(func=_copy)
    parser.add_argument('source_host', nargs='?', default=None, help='source host to connect to, may be an alias')
    parser.add_argument('source_user', nargs='?', default=None, help='user to connect as or target host alias')
    parser.add_argument('target_host', nargs='?', default=None, help='target host to connect to')
    parser.add_argument('target_user', nargs='?', default=None, help='user to connect as')
    _add_project(parser)
    _add_work_dir(parser)
    _add_clear(parser)
    _add_cv_assessor(parser)
    _add_common(parser)


####
def _host(args):
    cmd = CmdHost()
    cmd.run(args)


# push command
def _host_parser(subparsers):
    parser = subparsers.add_parser('host', help='host help')
    parser.set_defaults(func=_host)
    host_sub = parser.add_subparsers(dest='host_cmd', help='host subcommand help')
    add_parser = host_sub.add_parser('add', help='add help')
    add_parser.add_argument('alias', help='alias to add')
    add_parser.add_argument('url', help='url for alias')
    add_parser.add_argument('user', help='user for alias')
    rm_parser = host_sub.add_parser('rm', help='rm help')
    rm_parser.add_argument('alias', help='alias to remove')
    ls_parser = host_sub.add_parser('ls', help='ls help')
    _add_common(parser)


####
def _pull(args):
    cmd = CmdPull()
    cmd.run(args)


# pull command
def _pull_parser(subparsers):
    parser = subparsers.add_parser('pull', help='pull help')
    parser.set_defaults(func=_pull)
    _add_host_user(parser)
    _add_work_dir(parser)
    parser.add_argument('--no-cache', action='store_true', help='download fresh copy of data')
    parser.add_argument('--no-download', action='store_true', help='do not download the data, only the metadata')
    _add_project(parser)
    _add_cv_assessor(parser)
    _add_common(parser)


####
def _push(args):
    cmd = CmdPush()
    cmd.run(args)


# push command
def _push_parser(subparsers):
    parser = subparsers.add_parser('push', help='push help')
    parser.set_defaults(func=_push)
    _add_host_user(parser)
    _add_work_dir(parser)
    _add_clear(parser)
    _add_common(parser)


####
def _report(args):
    cmd = CmdReport()
    cmd.run(args)


# report command
def _report_parser(subparsers):
    parser = subparsers.add_parser('report', help='report help')
    parser.set_defaults(func=_report)
    _add_host_user(parser)
    parser.add_argument('--file', help='read server config from file, ignores connection info')
    _add_project(parser)
    _add_cv_assessor(parser)
    _add_common(parser)


####
def _verify(args):
    cmd = CmdVerify()
    cmd.run(args)


# copy command
def _verify_parser(subparsers):
    parser = subparsers.add_parser('verify', help='Verify data from source host on target host')
    parser.set_defaults(func=_verify)
    parser.add_argument('source_host', nargs='?', default=None, help='source host to connect to, may be an alias')
    parser.add_argument('source_user', nargs='?', default=None, help='user to connect as or target host alias')
    parser.add_argument('target_host', nargs='?', default=None, help='target host to connect to')
    parser.add_argument('target_user', nargs='?', default=None, help='user to connect as')
    _add_project(parser)
    _add_cv_assessor(parser)
    _add_common(parser)


####
def main():
    parser = argparse.ArgumentParser()
    parser.set_defaults(func=lambda def_args: parser.print_help())

    # sub-parsers for commands
    subparsers = parser.add_subparsers(help='sub-command help')
    _copy_parser(subparsers)
    _host_parser(subparsers)
    _pull_parser(subparsers)
    _push_parser(subparsers)
    _report_parser(subparsers)
    _verify_parser(subparsers)

    # Run command
    args = parser.parse_args()
    args.func(args)


####
if __name__ == '__main__':
    main()
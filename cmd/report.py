####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

import argparse
import datetime
from pathlib import Path

from server import ServerConfig, ServerReport
from server.io import JsonServerConfigReader, XnatServerConfigReader
from .single import CmdSingleServer


class CmdReport(CmdSingleServer):
    def __init__(self):
        CmdSingleServer.__init__(self, 'report')
        self._file: Path = None

    def run_command(self, args: argparse.Namespace):
        if not self._parse_args(args):
            exit()
        self._logger.info(f"{str.lower(self._app_name)} {self._cmd_name} startup")
        sc: ServerConfig = None
        if self._file:
            reader = JsonServerConfigReader(self._file, no_cv=self._no_cv, no_assessors=self._no_assessors,
                                            roi_only=self._roi_only)
            sc = reader.read()
        else:
            if not self._connect():
                exit()
            reader = XnatServerConfigReader(self._xnat, logger=self._logger, no_cv=self._no_cv,
                                            no_assessors=self._no_assessors, roi_only=self._roi_only)
            sc = reader.read()
        self._logger.info(f"Server config report for {self._xnat_config[self.URL]}")
        report = ServerReport(sc, uri=self._xnat_config[self.URL], date=datetime.datetime.now())
        report.display(recurse=True)

    def shutdown_command(self):
        self._single_shutdown()

    def _parse_args(self, args: argparse.Namespace) -> bool:
        if not CmdSingleServer._parse_args(self, args):
            return False
        if args.file:
            self._file = Path(args.file)
            if not self._file.exists():
                msg = f"File not found: {self._file}"
                print(msg)
                self._logger.error(msg)
                return False
        return True

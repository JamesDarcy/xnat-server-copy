####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.
from abc import ABC
import argparse
import json
from pathlib import Path
from typing import Dict, Set

import xnat

from .base import CmdBase


class CmdSingleServer(CmdBase, ABC):
    def __init__(self, cmd_name: str):
        CmdBase.__init__(self, cmd_name)
        self._config_req: Set[str] = set()
        self._xnat: xnat.session = None
        self._xnat_config: Dict[str, str] = {}
        # Populate required keys
        self._config_req.add(self.URL)
        self._config_req.add(self.USER)

    def _connect(self) -> bool:
        for key in self._config_req:
            if key not in self._xnat_config or not self._xnat_config[key]:
                msg = f"Error: {key} not specified for connection"
                print(msg)
                self._logger.error(msg)
                return False
        try:
            self._xnat = self._connect_server(self._xnat_config, "XNAT")
        except xnat.exceptions.XNATError as ex:
            msg = f"Error connecting to {self._xnat_config[self.URL]}: {ex}"
            print(msg)
            self._logger.error(msg)
            return False
        return True

    def _parse_args(self, args: argparse.Namespace) -> bool:
        if not self._parse_host_user(args):
            return False
        return True

    def _parse_host_user(self, args: argparse.Namespace) -> bool:
        if args.host:
            if args.user:
                self._xnat_config[self.URL] = args.host
                self._xnat_config[self.USER] = args.user
                return True
            else:
                return self._check_alias(args.host, self._xnat_config)
        msg = 'No host specified'
        print(msg)
        self._logger.error(msg)
        return False

    def _single_shutdown(self):
        if self._xnat is not None:
            self._xnat.disconnect()
            self._logger.info(f"XNAT disconnected: {self._xnat_config[self.USER]}@{self._xnat_config[self.URL]}")
        self._logger.info(f"{str.lower(self._app_name)} {self._cmd_name} shutdown")



####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

import argparse

from server.io import XnatDataVerifier
from .dual import CmdDualServer


class CmdVerify(CmdDualServer):
    def __init__(self):
        CmdDualServer.__init__(self, 'verify')
        self._projectId: str = None

    def run_command(self, args: argparse.Namespace):
        if not self._parse_args(args):
            exit()
        self._logger.info(f"{str.lower(self._app_name)} {self._cmd_name} startup")
        if not self._connect():
            exit()
        # Verify the data
        self._logger.info(f"Verifying data from {self._source_config[self.URL]} to {self._target_config[self.URL]}")
        verifier = XnatDataVerifier(self._source, self._target, logger=self._logger)
        if self._projectId:
            verifier.verify_project(self._projectId)
        else:
            verifier.verify()

    def shutdown_command(self):
        self._dual_shutdown()

    def _parse_args(self, args: argparse.Namespace) -> bool:
        if not CmdDualServer._parse_args(self, args):
            return False
        if args.project:
            self._projectId = args.project
        return True

####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

import argparse
import os
from pathlib import Path
import shutil

from server import ServerConfig
from server.io import XnatDataDownloader, XnatServerConfigReader
from .single import CmdSingleServer


class CmdPull(CmdSingleServer):
    def __init__(self):
        CmdSingleServer.__init__(self, 'pull')
        self._work_dir: Path = None
        self._no_download = False
        self._projectId: str = None

    def run_command(self, args: argparse.Namespace):
        if not self._parse_args(args):
            exit()
        self._logger.info(f"{str.lower(self._app_name)} {self._cmd_name} startup")
        if not self._connect():
            exit()
        if not self._use_cache:
            self._clear_work_dir()
        self._logger.info(f"Reading ServerConfig from {self._xnat_config[self.URL]}")
        reader = XnatServerConfigReader(self._xnat, logger=self._logger, no_cv=self._no_cv,
                                        no_assessors=self._no_assessors, roi_only=self._roi_only)
        sc = reader.read()
        self._write(sc, self._work_dir)
        if not self._no_download:
            downloader = XnatDataDownloader(self._xnat, logger=self._logger, project=self._projectId,
                                            use_cache=self._use_cache)
            self._logger.info(f"Downloading data from {self._xnat_config[self.URL]}")
            downloader.download(sc, self._work_dir)

    def shutdown_command(self):
        self._single_shutdown()

    def _clear_work_dir(self):
        is_windows = os.name == 'nt'
        # Removing a non-empty tree often fails on Windows: https://bugs.python.org/issue33240
        for path in self._work_dir.iterdir():
            if path.is_dir():
                shutil.rmtree(path, ignore_errors=is_windows)
            else:
                path.unlink()

    def _parse_args(self, args: argparse.Namespace) -> bool:
        if not CmdSingleServer._parse_args(self, args):
            return False
        self._use_cache = not args.no_cache
        if args.work_dir:
            self._work_dir = Path(args.work_dir)
            if not self._work_dir.exists():
                self._work_dir.mkdir(mode=0o755, parents=True)
                self._logger.debug(f"Created working directory: {self._work_dir}")
        else:
            self._work_dir = Path('.')
        self._no_download = args.no_download
        if args.project:
            self._projectId = args.project
        return True

    def _write(self, sc: ServerConfig, path: Path):
        if not path.exists():
            path.mkdir()
        file = path / 'serverconfig.json'
        with file.open(mode='w') as target:
            target.write(sc.json_str())
            self._logger.info(f"JSON written to {file}")

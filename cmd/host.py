####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

import argparse
import json
from pathlib import Path

from .single import CmdBase


class CmdHost(CmdBase):
    ADD = 'add'
    LS = 'ls'
    RM = 'rm'

    def __init__(self):
        CmdBase.__init__(self, 'host')
        self._aliasname: str = None
        self._cmd: str = None
        self._url: str = None
        self._user: str = None

    def run_command(self, args: argparse.Namespace):
        if not self._parse_args(args):
            exit()
        self._logger.info(f"{str.lower(self._app_name)} {self._cmd_name} startup")
        if self._cmd == self.ADD:
            self._add()
        elif self._cmd == self.RM:
            self._rm()
        elif self._cmd == self.LS:
            self._ls()

    def shutdown_command(self):
        pass

    def _parse_args(self, args: argparse.Namespace) -> bool:
        self._cmd = args.host_cmd
        if self._cmd == self.ADD:
            self._aliasname = args.alias
            self._url = args.url
            self._user = args.user
        elif self._cmd == self.RM:
            self._aliasname = args.alias
        elif self._cmd == self.LS:
            pass
        else:
            self._logger.error(f"Unknown host command: {self._cmd}")
            return False
        return True

    def _add(self):
        self._aliases[self._aliasname] = {self.URL: self._url, self.USER: self._user}
        self._write_aliases()

    def _ls(self):
        for key, item in sorted(self._aliases.items()):
            print(f"{key}: {item[self.URL]} {item[self.USER]}")

    def _rm(self):
        if self._aliasname in self._aliases:
            del self._aliases[self._aliasname]
            self._write_aliases()

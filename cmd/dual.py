####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.
from abc import ABC
import argparse
import json
from pathlib import Path
from typing import Dict, Set

import xnat

from .base import CmdBase


class CmdDualServer(CmdBase, ABC):
    def __init__(self, cmd_name: str):
        CmdBase.__init__(self, cmd_name)
        self._config_req: Set[str] = set()
        self._source: xnat.session = None
        self._source_config: Dict[str, str] = {}
        self._target: xnat.session = None
        self._target_config: Dict[str, str] = {}
        # Populate required keys
        self._config_req.add(self.URL)
        self._config_req.add(self.USER)

    def _connect(self) -> bool:
        if not self._check_config_req(self._config_req, self._source_config, 'Source') \
                or not self._check_config_req(self._config_req, self._target_config, 'Target'):
            return False
        try:
            self._source = self._connect_server(self._source_config, 'Source')
        except xnat.exceptions.XNATError as ex:
            msg = f"Error connecting to {self._source_config[self.URL]}: {ex}"
            print(msg)
            self._logger.error(msg)
            return False
        try:
            self._target = self._connect_server(self._target_config, 'Target')
        except xnat.exceptions.XNATError as ex:
            msg = f"Error connecting to {self._target_config[self.URL]}: {ex}"
            print(msg)
            self._logger.error(msg)
            return False
        return True

    def _dual_shutdown(self):
        if self._source is not None:
            self._source.disconnect()
            self._logger.info(f"Source disconnected: {self._source_config[self.USER]}@{self._source_config[self.URL]}")
        if self._target is not None:
            self._target.disconnect()
            self._logger.info(f"Target disconnected: {self._target_config[self.USER]}@{self._target_config[self.URL]}")
        self._logger.info(f"{str.lower(self._app_name)} {self._cmd_name} shutdown")

    def _parse_args(self, args: argparse.Namespace) -> bool:
        if not self._parse_host_user(args):
            return False
        return True

    def _parse_host_user(self, args: argparse.Namespace) -> bool:
        if args.source_host and args.source_user:
            if args.target_host and args.target_user:
                self._source_config[self.URL] = args.source_host
                self._source_config[self.USER] = args.source_user
                self._target_config[self.URL] = args.target_host
                self._target_config[self.USER] = args.target_user
                return True
            else:
                return self._check_alias(args.source_host, self._source_config) \
                        and self._check_alias(args.source_user, self._target_config)
        msg = 'Two hosts must be specified'
        print(msg)
        self._logger.error(msg)
        return False

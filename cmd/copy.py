####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

import argparse
from pathlib import Path

from server import ServerConfig
from server.io import XnatDataCopier, XnatDataVerifier, XnatDeleter, XnatServerConfigReader
from .dual import CmdDualServer


class CmdCopy(CmdDualServer):
    def __init__(self):
        CmdDualServer.__init__(self, 'copy')
        self._clear: bool = False
        self._projectId: str = None
        self._work_dir: Path = None

    def run_command(self, args: argparse.Namespace):
        if not self._parse_args(args):
            exit()
        self._logger.info(f"{str.lower(self._app_name)} {self._cmd_name} startup")
        if not self._connect():
            exit()
        # Fetch source metadata
        msg = f"Reading ServerConfig from {self._source_config[self.URL]}"
        if self._projectId:
            msg = f"{msg} - {self._projectId}"
        self._logger.info(msg)
        reader = XnatServerConfigReader(self._source, project=self._projectId, logger=self._logger, no_cv=self._no_cv,
                                        no_assessors=self._no_assessors, roi_only=self._roi_only)
        sc = reader.read()
        # Clear existing data if required
        if self._clear:
            self._logger.info(f"Clearing existing data on {self._target_config[self.URL]}")
            deleter = XnatDeleter(self._target, logger=self._logger)
            if self._projectId:
                deleter.clear_project(self._projectId)
            else:
                deleter.clear_server()
        # Copy the actual data
        self._logger.info(f"Copying data from {self._source_config[self.URL]} to {self._target_config[self.URL]}")
        copier = XnatDataCopier(self._source, self._target, logger=self._logger)
        copier.copy(sc, self._work_dir)
        # Verify the data
        self._logger.info(f"Verifying data from {self._source_config[self.URL]} to {self._target_config[self.URL]}")
        verifier = XnatDataVerifier(self._source, self._target, logger=self._logger)
        if self._projectId:
            verifier.verify_project(self._projectId)
        else:
            verifier.verify()

    def shutdown_command(self):
        self._dual_shutdown()

    def _parse_args(self, args: argparse.Namespace) -> bool:
        if not CmdDualServer._parse_args(self, args):
            return False
        self._clear = args.clear
        if args.project:
            self._projectId = args.project
        if args.work_dir:
            self._work_dir = Path(args.work_dir)
            if not self._work_dir.exists():
                self._work_dir.mkdir(mode=0o755, parents=True)
                self._logger.debug(f"Created working directory: {self._work_dir}")
        else:
            self._work_dir = Path('.')
        return True

    def _write(self, sc: ServerConfig, path: Path):
        if not path.exists():
            path.mkdir()
        file = path / 'serverconfig.json'
        with file.open(mode='w') as target:
            target.write(sc.json_str())
            self._logger.info(f"JSON written to {file}")

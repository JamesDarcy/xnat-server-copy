####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

import argparse
from pathlib import Path

from server import ServerConfig
from server.io import JsonServerConfigReader, XnatDataUploader, XnatDeleter, XnatServerConfigWriter
from .single import CmdSingleServer


class CmdPush(CmdSingleServer):
    def __init__(self):
        CmdSingleServer.__init__(self, 'push')
        self._clear: bool = False
        self._work_dir: Path = None

    def run_command(self, args: argparse.Namespace):
        if not self._parse_args(args):
            exit()
        self._logger.info(f"{str.lower(self._app_name)} {self._cmd_name} startup")
        if not self._connect():
            exit()
        if self._clear:
            deleter = XnatDeleter(self._xnat)
            deleter.clear_server()
        self._config_server()

    def shutdown_command(self):
        self._single_shutdown()

    def _config_server(self):
        file = self._work_dir / 'serverconfig.json'
        sc: ServerConfig = JsonServerConfigReader(file).read()
        self._logger.info(f"ServerConfig loaded from {file}")
        writer = XnatServerConfigWriter(self._xnat, logger=self._logger)
        self._logger.info(f"Writing ServerConfig core to {self._xnat_config[self.URL]}")
        writer.write_core(sc)
        uploader = XnatDataUploader(self._xnat, logger=self._logger)
        self._logger.info(f"Uploading data to {self._xnat_config[self.URL]}")
        uploader.upload(sc, self._work_dir)
        self._logger.info(f"Writing ServerConfig shares to {self._xnat_config[self.URL]}")
        writer.write_shares(sc)

    def _parse_args(self, args: argparse.Namespace) -> bool:
        if not CmdSingleServer._parse_args(self, args):
            return False
        self._clear = args.clear
        if args.work_dir:
            self._work_dir = Path(args.work_dir)
            if not self._work_dir.exists():
                self._work_dir.mkdir(mode=0o755, parents=True)
                self._logger.debug(f"Created working directory: {self._work_dir}")
        else:
            self._work_dir = Path('.')
        return True

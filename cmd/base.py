####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

from abc import ABC, abstractmethod
import argparse
import getpass
import json
import logging
from pathlib import Path
import platform
import sys
from typing import Dict, Set

import xnat


class CmdBase(ABC):
    ADD = 'add'
    DEBUG = 'DEBUG'
    ERROR = 'ERROR'
    INFO = 'INFO'
    LS = 'ls'
    RM = 'rm'
    URL = 'url'
    USER = 'user'
    WARNING = 'WARNING'

    def __init__(self, cmd_name: str):
        # _app_name must be defined before use
        self._app_name: str = 'xPync'
        self._aliases = {}
        self._aliasFile: Path = self.get_app_dir() / 'aliases.json'
        self._alias_req_keys = {self.URL, self.USER}
        if not cmd_name:
            raise TypeError('Command name must not be None or empty')
        self._cmd_name: str = cmd_name
        self._logger: logging.Logger
        self._log_console: bool = False
        self._log_level: int = logging.INFO
        self._log_xnat: bool = False
        self._no_assessors: bool = False
        self._no_cv: bool = False
        self._roi_only: bool = False
        self._create_app_dir()

    def run(self, args: argparse.Namespace):
        try:
            if not self._base_parse_args(args):
                exit()
            self._init_logging()
            self._read_aliases()
            self.run_command(args)
        except RuntimeError:
            logging.critical(sys.exc_info()[0])
        finally:
            self._shutdown()

    def get_app_dir(self) -> Path:
        home = Path.home()
        osType = platform.system()
        if osType == 'Windows':
            return home / 'AppData' / 'Local' / self._app_name
        else:
            return home / f".{str.lower(self._app_name)}"

    @abstractmethod
    def run_command(self, args: argparse.Namespace):
        pass

    @abstractmethod
    def shutdown_command(self):
        pass

    def _alias_has_keys(self, item: dict):
        for key in self._alias_req_keys:
            if key not in item:
                return False
        return True

    def _base_parse_args(self, args):
        self._log_console = args.log_console
        if args.log_level == self.DEBUG:
            self._log_level = logging.DEBUG
        elif args.log_level == self.WARNING:
            self._log_level = logging.WARNING
        elif args.log_level == self.ERROR:
            self._log_level = logging.ERROR
        else:
            self._log_level = logging.INFO
        self._log_xnat = args.log_xnat
        self._cv_assessor_parse_args(args)
        return True

    def _check_alias(self, alias_name: str, config: Dict[str, str]) -> bool:
        if alias_name in self._aliases:
            # Alias has been used
            alias = self._aliases[alias_name]
            config[self.URL] = alias[self.URL]
            config[self.USER] = alias[self.USER]
            return True
        else:
            msg = f"Host alias not found: {alias_name}"
            print(msg)
            self._logger.error(msg)
            return False

    def _check_config_req(self, req: Set[str], config: Dict[str, str], name='XNAT'):
        for key in req:
            if key not in config or not config[key]:
                msg = f"Error: {key} not specified for {name} connection"
                print(msg)
                self._logger.error(msg)
                return False
        return True

    def _connect_server(self, config, name: str) -> xnat.session:
        try:
            password = getpass.getpass(f"Password for {config[self.USER]}@{config[self.URL]}: ")
        except KeyboardInterrupt:
            print()
            raise xnat.exceptions.XNATError("User cancelled login")
        logger = self._logger if self._log_xnat else None
        conn = xnat.connect(config[self.URL], user=config[self.USER], password=password, logger=logger)
        self._logger.info(f"{name} connected: {config[self.USER]}@{config[self.URL]}")
        return conn

    def _create_app_dir(self):
        app_dir = self.get_app_dir()
        if not app_dir.exists():
            app_dir.mkdir(mode=0o750, parents=True)

    def _cv_assessor_parse_args(self, args):
        if hasattr(args, 'no_cv'):
            self._no_cv = args.no_cv
        if hasattr(args, 'no_assessors'):
            self._no_assessors = args.no_assessors
        if hasattr(args, 'roi_only'):
            self._roi_only = args.roi_only
        return True

    def _init_logging(self):
        name = self._app_name.lower()
        log_format = "%(asctime)s %(name)-12s %(levelname)-8s %(message)s"
        logging.basicConfig(
            format=log_format,
            datefmt="%y-%m-%d %H:%M:%S",
            filename=f"{name}.log")
        self._logger = logging.getLogger(name)
        self._logger.setLevel(self._log_level)
        if self._log_console:
            console = logging.StreamHandler()
            console.setLevel(self._log_level)
            console.setFormatter(logging.Formatter(log_format))
            self._logger.addHandler(console)
        file = logging.FileHandler(self.get_app_dir() / f"{name}.log")
        file.setLevel(self._log_level)
        file.setFormatter(logging.Formatter(log_format))
        self._logger.addHandler(file)

    def _read_aliases(self):
        if not self._aliasFile.exists():
            self._logger.debug('Aliases file not found')
            return
        with self._aliasFile.open('r') as file:
            aliases = json.load(file)
        if type(aliases) is not dict:
            return
        for key, item in aliases.items():
            if self._alias_has_keys(item):
                self._aliases[key] = item
        self._logger.debug(f"{len(self._aliases)} aliases loaded")

    def _shutdown(self):
        self.shutdown_command()
        logging.shutdown()

    def _write_aliases(self):
        with self._aliasFile.open('w') as file:
            json.dump(self._aliases, file)

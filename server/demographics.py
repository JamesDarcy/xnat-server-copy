####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

from typing import Dict, FrozenSet


class Demographics:
    keys: FrozenSet[str] = {'age', 'birth_weight', 'dob', 'education', 'education_desc', 'employment', 'ethnicity', 'gender',
                'gestational_age', 'handedness', 'height', 'post_menstrual_age', 'race', 'race2', 'race3', 'race4',
                'race5', 'race6', 'ses', 'weight', 'yob'}

    @staticmethod
    def new_item(item: Dict[str, any]):
        demogfx = Demographics(item)
        return demogfx if not demogfx.is_empty() else None

    def __init__(self, demogfx: Dict[str, any]):
        if not demogfx:
            raise TypeError('Dictionary must not be None or empty')
        self._demogfx: Dict[str, any] = {}
        for key in Demographics.keys:
            if key in demogfx:
                self._demogfx[key] = demogfx[key]

    def __repr__(self):
        return f"{self.__class__.__name__}(demograhics={self._demogfx!r}"

    def json_str(self, indent: str = "") -> str:
        if indent is None:
            indent = ""
        pad = indent+"  "
        if not self._demogfx:
            return f"{indent}{{}}"
        out = f"{indent}{{\n"
        data_list = []
        for key, value in self._demogfx.items():
            if type(value) == str:
                data_list.append(f"{pad}\"{key}\": \"{value}\"")
            else:
                data_list.append(f"{pad}\"{key}\": {value}")
        separator = ',\n'
        out += separator.join(data_list)
        out += f"\n{indent}}}"
        return out

    def is_empty(self):
        return not self._demogfx

    @property
    def items(self):
        return sorted(self._demogfx.items())


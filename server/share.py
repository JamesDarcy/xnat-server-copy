#!/usr/bin/env python

####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

from typing import Dict


class Share:
    @staticmethod
    def new(share: Dict[str, str]):
        return Share(share.get('project', None), share.get('label', None))

    @staticmethod
    def new_item(item: Dict[str, dict]):
        data: Dict[str, str] = item['data_fields']
        return Share(data.get('project', None), data.get('label', None))

    def __init__(self, project: str, label: str):
        if not project:
            raise TypeError('Project must not be None or empty')
        if not label:
            raise TypeError('Label must not be None or empty')
        self._project: str = project
        self._label: str = label

    def __repr__(self):
        return f"{self.__class__.__name__}(project={self._project!r}, label={self._label!r}"

    def json_str(self, indent: str = "", xnat: bool = False) -> str:
        if indent is None:
            indent = ""
        pad = indent+"  "
        out = (f"{indent}{{\n"
               f"{pad}\"project\": \"{self._project}\",\n"
               f"{pad}\"label\": \"{self._label}\"\n"
               f"{indent}}}")
        return out

    @property
    def label(self):
        return self._label

    @property
    def project(self):
        return self._project

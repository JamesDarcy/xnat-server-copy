#!/usr/bin/env python

####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

from typing import Dict, List

from .projectuser import ProjectUser
from .resourcedir import ResourceDir
from .resourcefile import ResourceFile
from .subject import Subject


class Project:
    @staticmethod
    def new(proj: Dict[str, str]):
        project = Project(proj.get("ID", None), title=proj.get("name", ""), running_title=proj.get("secondary_ID", ""),
                          description=proj.get("description", ""), pi_firstname=proj.get("pi_firstname", ""),
                          pi_lastname=proj.get("pi_lastname", ""))
        if 'accesssiblity' in proj:
            project.accessibility = proj['acceessbility']
        if 'prearchive_code' in proj:
            project.prearchive_code = proj['prearchive_code']
        if 'quarantine_code' in proj:
            project.quarantine_code = proj['quarantine_code']
        return project

    def __init__(self, project_id: str, title: str = "", running_title: str = "", description: str = "",
                 pi_firstname: str = "", pi_lastname: str = ""):
        if (project_id is None) or (project_id == ""):
            raise TypeError("project_id must not be None or empty")
        self._id = project_id
        if (title is None) or (title == ""):
            self.title = self._id
        else:
            self.title = title
        if (running_title is None) or (running_title == ""):
            self.running_title = self._id
        else:
            self.running_title = running_title
        self.description = "" if description is None else description
        self.pi_firstname = "" if pi_firstname is None else pi_firstname
        self.pi_lastname = "" if pi_lastname is None else pi_lastname
        # Initialise remaining members for later use
        self._accessibility: str = 'private'
        self._prearchive_code: int = 4
        self._quarantine_code: int = 0
        self._rsrc_dirs: Dict[str, ResourceDir] = {}
        self._rsrc_files: Dict[str, ResourceFile] = {}
        self._subjects: Dict[str, Subject] = {}
        self._users: Dict[str, ProjectUser] = {}

    def __repr__(self):
        return (f"{self.__class__.__name__}(id={self._id!r}, title={self.title!r}, "
                f"running_title={self.running_title!r}, description={self.description!r}, "
                f"pi_firstname={self.pi_firstname!r}, pi_lastname={self.pi_lastname!r}, "
                f"accessibility={self._accessibility}, prearchive_code={self._prearchive_code!r}, "
                f"quarantine_code={self._quarantine_code!r})")

    @property
    def accessibility(self):
        return self._accessibility

    @accessibility.setter
    def accessibility(self, access: str):
        if access not in ['private', 'protected', 'public']:
            raise ValueError(f"Accessibility must be one of: 'private', 'protected', 'public'")
        self._accessibility = access

    def add_resource_dir(self, rsrc_dir: ResourceDir):
        if rsrc_dir is None:
            raise TypeError("ResourceDir must not be None")
        self._rsrc_dirs[rsrc_dir.label] = rsrc_dir

    def add_resource_file(self, rsrc_file: ResourceFile):
        if rsrc_file is None:
            raise TypeError("ResourceDir must not be None")
        self._rsrc_files[rsrc_file.name] = rsrc_file

    def add_subject(self, subject: Subject):
        if subject is None:
            raise TypeError("Subject must not be None")
        if subject.project != self._id:
            raise ValueError("Subject not part of this project")
        self._subjects[subject.label] = subject

    def add_user(self, user: ProjectUser):
        if user is None:
            raise TypeError("User must not be None")
        prefix = self._id+"_"
        if not user.group_id.startswith(prefix):
            raise ValueError("User not part of this project")
        self._users[user.username] = user

    def add_users(self, users: Dict[str, ProjectUser]):
        if users is None:
            raise TypeError("Users must not be None")
        for _, user in users.items():
            self.add_user(user)

    @property
    def id(self):
        return self._id

    def json_str(self, indent: str = "", xnat: bool = False) -> str:
        if indent is None:
            indent = ""
        pad = indent+"  "
        dpad = pad+"  "
        out = f"{indent}{{\n"
        out += (f"{pad}\"ID\": \"{self._id}\",\n"
                f"{pad}\"name\": \"{self.title}\",\n"
                f"{pad}\"secondary_ID\": \"{self.running_title}\",\n"
                f"{pad}\"description\": \"{self.description}\",\n"
                f"{pad}\"pi_firstname\": \"{self.pi_firstname}\",\n"
                f"{pad}\"pi_lastname\": \"{self.pi_lastname}\"")
        if xnat:
            out += "\n"
        else:
            out += f",\n{pad}\"accessibility\": \"{self._accessibility}\""
            out += f",\n{pad}\"prearchive_code\": {self._prearchive_code}"
            out += f",\n{pad}\"quarantine_code\": {self._quarantine_code}"
            separator = ",\n"
            # Users
            out += f",\n{pad}\"users\":\n{pad}{{"
            user_list = []
            for username, user in self.users:
                user_list.append(f"{dpad}\"{username}\":\n{user.json_str(dpad)}")
            if user_list:
                out += f"\n{separator.join(user_list)}\n{pad}}}"
            else:
                out += f"}}"
            # Subjects
            out += f",\n{pad}\"subjects\":\n{pad}{{"
            subj_list = []
            for subj_label, subj in self.subjects:
                subj_list.append(f"{dpad}\"{subj_label}\":\n{subj.json_str(dpad)}")
            if subj_list:
                out += f"\n{separator.join(subj_list)}\n{pad}}}"
            else:
                out += f"}}\n"
            # ResourceDirs
            if len(self._rsrc_dirs) > 0:
                out += f",\n{pad}\"resource_dirs\":\n{pad}{{"
                dir_list = []
                for dir_label, rsrc_dir in self.resource_dirs:
                    dir_list.append(f"{dpad}\"{dir_label}\":\n{rsrc_dir.json_str(dpad)}")
                if dir_list:
                    out += f"\n{separator.join(dir_list)}\n{pad}}}"
                else:
                    out += f"}}\n"
            # ResourceFiles
            if len(self._rsrc_files) > 0:
                out += f",\n{pad}\"resource_files\":\n{pad}{{"
                file_list = []
                for filename, rsrc_file in self.resource_files:
                    file_list.append(f"{dpad}\"{filename}\":\n{rsrc_file.json_str(dpad)}")
                if file_list:
                    out += f"\n{separator.join(file_list)}\n{pad}}}\n"
                else:
                    out += f"}}\n"
        out += f"{indent}}}"
        return out

    @property
    def prearchive_code(self):
        return self._prearchive_code

    @prearchive_code.setter
    def prearchive_code(self, code: int):
        if code not in [0, 4, 5]:
            raise ValueError('Prearchive_code must be one of: 0, 4, 5')
        self._prearchive_code = code

    @property
    def quarantine_code(self):
        return self._quarantine_code

    @quarantine_code.setter
    def quarantine_code(self, code: int):
        if code not in [0, 1]:
            raise ValueError('Quarantine_code must be one of: 0, 1')
        self._quarantine_code = code

    @property
    def resource_dirs(self):
        return sorted(self._rsrc_dirs.items())

    @property
    def resource_files(self):
        return sorted(self._rsrc_files.items())

    @property
    def subjects(self):
        return sorted(self._subjects.items())

    @property
    def users(self):
        return sorted(self._users.items())

####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

from typing import Dict

from .share import Share


class Displayable:
    def __init__(self):
        pass

    def __repr__(self):
        return f"{self.__class__.__name__}"

    def display(self, indent: str = "", recurse: bool = False):
        if not indent:
            indent = ""
        print(f"{indent}{self.__class__.__name__}")

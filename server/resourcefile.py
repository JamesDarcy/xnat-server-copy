#!/usr/bin/env python

####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

from typing import Dict


class ResourceFile:
    @staticmethod
    def new(file: Dict[str, str]):
        return ResourceFile(file.get('Name', None), category_id=file.get('cat_ID', None),
                            collection=file.get('collection', ''), content=file.get('file_content', ''),
                            format=file.get('file_format', ''), tags=file.get('file_tags', ''))

    # @staticmethod
    # def new_item(item: Dict[str, dict]):
    #     meta: Dict[str, str] = item['meta']
    #     data: Dict[str, str] = item['data_fields']
    #     return ResourceFile(data.get('label', None), meta.get('xsi:type', None))

    def __init__(self, name: str, category_id: str, collection: str = '', content: str = '', format: str = '',
                 tags: str = ''):
        if not name:
            raise TypeError('Name must not be None or empty')
        if not category_id:
            raise TypeError('Category ID must not be None or empty')
        if not collection:
            collection = ''
        self._name: str = name
        self._category_id: str = category_id
        self._collection: str = collection
        self.content: str = content
        self.format: str = format
        self.tags: str = tags

    def __repr__(self):
        return (f"{self.__class__.__name__}(name={self._name!r}, category_id={self._category_id!r}, "
                f"collection={self._collection!r}, content={self.content!r}, format={self.format!r}, "
                f"tags={self.tags!r}")

    @property
    def category_id(self):
        return self._category_id

    @property
    def collection(self):
        return self._collection

    def json_str(self, indent: str = "", xnat: bool = False) -> str:
        if indent is None:
            indent = ""
        pad = indent+"  "
        out = f"{indent}{{\n"
        prop_list = [f"{pad}\"Name\": \"{self._name}\"",
                     f"{pad}\"cat_ID\": \"{self._category_id}\""]
        if self._collection:
            prop_list.append(f"{pad}\"collection\": \"{self._collection}\"")
        if self.content:
            prop_list.append(f"{pad}\"file_content\": \"{self.content}\"")
        if self.format:
            prop_list.append(f"{pad}\"file_format\": \"{self.format}\"")
        if self.tags:
            prop_list.append(f"{pad}\"file_tags\": \"{self.tags}\"")
        separator = ',\n'
        out += separator.join(prop_list)
        out += f"\n{indent}}}"
        return out

    @property
    def name(self):
        return self._name

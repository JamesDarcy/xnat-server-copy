####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

import datetime
from typing import Dict, List, Set

from .displayable import Displayable
from .experiment import Experiment
from .project import Project
from .serverconfig import ServerConfig
from .subject import Subject
from .xsitype import XsiType


class BaseReport:
    def __init__(self):
        self._has_custom_vars: bool = False
        self._has_issues: bool = False
        self._modalities: Set[str] = set()

    def __repr__(self):
        return f"{self.__class__.__name__}"

    @property
    def has_custom_variables(self) -> bool:
        return self._has_custom_vars

    @property
    def has_issues(self) -> bool:
        return self._has_issues

    @property
    def modalities(self) -> List[str]:
        return sorted(self._modalities)

    def _check_custom(self, item):
        if item.has_custom_variables:
            self._has_custom_vars = True
            self._has_issues = True

    def _filter_modalities(self):
        standard = {'CT', 'MR', 'PET', 'PETMR'}
        non_standard: Set[str] = set()
        for modality in self._modalities:
            if modality not in standard:
                non_standard.add(modality)
        self._modalities = non_standard
        self._has_issues |= len(self._modalities) > 0

    def _merge_modalities(self, modalities):
        for modality in modalities:
            if modality not in self._modalities:
                self._modalities.add(modality)


class TypeReport(BaseReport):
    def __init__(self):
        BaseReport.__init__(self)
        self._no_label: int = 0
        self._shares: Set[str] = set()

    def __repr__(self):
        return f"{self.__class__.__name__}"

    @property
    def no_label_resources(self) -> int:
        return self._no_label

    @property
    def shares(self) -> List[str]:
        return sorted(self._shares)

    def _process_modality(self, item):
        modality = XsiType.modality(item.xsi_type)
        if modality not in self._modalities:
            self._modalities.add(modality)

    def _process_resources(self, item):
        for _, res_file in item.resource_files:
            if not res_file.collection:
                self._no_label += 1
                self._has_issues = True

    def _process_shares(self, item):
        for _, share in item.shares:
            if share.project not in self._shares:
                self._shares.add(share.project)
            self._has_issues = True


class ServerReport(BaseReport, Displayable):
    @staticmethod
    def new(sc: ServerConfig, uri: str = None):
        if not sc:
            raise TypeError('ServerConfig must not be None')
        sc_rep = ServerReport(sc)
        return sc_rep

    def __init__(self, sc: ServerConfig = None, uri: str = None, date: datetime = None):
        BaseReport.__init__(self)
        Displayable.__init__(self)
        if not sc:
            raise TypeError('ServerConfig must not be None or empty')
        self._uri = uri
        self._date = date if date else None
        self._has_issues: bool = False
        self._invs: int = len(sc.users)
        self._users: int = len(sc.investigators)
        self._project_reps: Dict[str, ProjectReport] = {}
        self._custom_projects: Set[str] = set()
        for _, project in sc.projects:
            self._process_project(project)
        self._filter_modalities()

    def __repr__(self):
        return f"{self.__class__.__name__}"

    def display(self, indent: str = "", recurse: bool = False):
        if not indent:
            indent = ""
        pad = indent+'* '
        output = f"{indent}{self.__class__.__name__}\n"
        if self._uri:
            output += f"{pad}Uri: {self._uri}\n"
        if self._date:
            output += f"{pad}Date: {self._date}\n"
        if self._users > 0:
            output += f"{pad}Users: {self._users}\n"
        if self._invs > 0:
            output += f"{pad}Investigators: {self._invs}\n"
        if self._has_custom_vars:
            output += f"{pad}Custom Variables: YES - {', '.join(self._custom_projects)}\n"
        if len(self._modalities) > 0:
            output += f"{pad}Non-Standard Modalities: {', '.join(self._modalities)}\n"
        output += f"{pad}Projects: {len(self._project_reps)}"
        print(output)
        if recurse:
            dpad = indent+'  '
            for _, proj_rep in self.project_reports:
                if proj_rep.has_issues:
                    proj_rep.display(dpad, recurse)

    @property
    def investigators(self) -> int:
        return self._invs

    @property
    def project_reports(self) -> List:
        return sorted(self._project_reps.items())

    @property
    def users(self):
        return self._users

    def _check_custom(self, proj_rep):
        BaseReport._check_custom(self, proj_rep)
        if proj_rep.has_custom_variables:
            self._custom_projects.add(proj_rep.id())

    def _process_project(self, project: Project):
        proj_rep = ProjectReport.new(project)
        self._has_issues |= proj_rep.has_issues
        self._project_reps[project.id] = proj_rep
        self._merge_modalities(proj_rep.modalities)
        self._check_custom(proj_rep)


class ProjectReport(TypeReport, Displayable):
    @staticmethod
    def new(project: Project):
        if not project:
            raise TypeError('Project must not be None')
        proj_rep = ProjectReport(project)
        return proj_rep

    def __init__(self, project: Project = None):
        TypeReport.__init__(self)
        Displayable.__init__(self)
        if not project:
            raise TypeError('Project must not be None or empty')
        self._id: str = project.id
        self._subject_reps: Dict[str, SubjectReport] = {}
        self._process_resources(project)
        for _, subject in project.subjects:
            self._process_subject(subject)
        self._filter_modalities()

    def __repr__(self):
        return f"{self.__class__.__name__}(ID={self._id!r}"

    def display(self, indent: str = "", recurse: bool = False):
        if not indent:
            indent = ""
        pad = indent+'* '
        output = f"{indent}{self.__class__.__name__}\n{pad}ID: {self._id}\n"
        if self._no_label > 0:
            output += f"{pad}NO LABEL Resources: {self._no_label}\n"
        if self._has_custom_vars:
            output += f"{pad}Custom Variables: YES\n"
        if len(self._shares) > 0:
            output += f"{pad}Shares: {', '.join(self._shares)}\n"
        if len(self._modalities) > 0:
            output += f"{pad}Non-Standard Modalities: {', '.join(self._modalities)}\n"
        output += f"{pad}Subjects: {len(self._subject_reps)}"
        print(output)
        if recurse:
            dpad = indent+'  '
            for _, subj_rep in self.subject_reports:
                if subj_rep.has_issues:
                    subj_rep.display(dpad, recurse)

    def id(self) -> str:
        return self._id

    @property
    def subject_reports(self) -> List:
        return sorted(self._subject_reps.items())

    def _process_subject(self, subject: Subject):
        self._process_shares(subject)
        subj_rep = SubjectReport.new(subject)
        self._has_issues |= subj_rep.has_issues
        self._subject_reps[subject.label] = subj_rep
        self._merge_modalities(subj_rep.modalities)
        self._check_custom(subj_rep)


class SubjectReport(TypeReport, Displayable):
    @staticmethod
    def new(subject: Subject):
        if not subject:
            raise TypeError('Subject must not be None')
        subj_rep = SubjectReport(subject)
        return subj_rep

    def __init__(self, subject: Subject = None):
        TypeReport.__init__(self)
        Displayable.__init__(self)
        if not subject:
            raise TypeError('Subject must not be None or empty')
        self._label: str = subject.label
        self._expt_reps: Dict[str, ExperimentReport] = {}
        self._process_resources(subject)
        self._process_shares(subject)
        for _, expt in subject.experiments:
            self._process_expt(expt)
        self._filter_modalities()
        if len(subject.custom_variables) > 0:
            self._has_custom_vars = True
            self._has_issues = True

    def __repr__(self):
        return f"{self.__class__.__name__}(Label={self._label!r}"

    def display(self, indent: str = "", recurse: bool = False):
        if not indent:
            indent = ""
        pad = indent+'* '
        output = f"{indent}{self.__class__.__name__}\n{pad}Label: {self._label}\n"
        if self._no_label > 0:
            output += f"{pad}NO LABEL Resources: {self._no_label}\n"
        if self._has_custom_vars:
            output += f"{pad}Custom Variables: YES\n"
        if len(self._shares) > 0:
            output += f"{pad}Shares: {', '.join(self._shares)}\n"
        if len(self._modalities) > 0:
            output += f"{pad}Non-Standard Modalities: {', '.join(self._modalities)}\n"
        output += f"{pad}Experiments: {len(self._expt_reps)}"
        print(output)
        if recurse:
            dpad = indent+'  '
            for _, expt_rep in self.experiment_reports:
                if expt_rep.has_issues:
                    expt_rep.display(dpad, recurse)

    @property
    def experiment_reports(self):
        return sorted(self._expt_reps.items())

    @property
    def label(self) -> str:
        return self._label

    def _process_expt(self, expt: Experiment):
        expt_rep = ExperimentReport.new(expt)
        self._has_issues |= expt_rep.has_issues
        self._expt_reps[expt.label] = expt_rep
        self._process_modality(expt)
        self._merge_modalities(expt_rep.modalities)
        self._check_custom(expt_rep)


class ExperimentReport(TypeReport, Displayable):
    @staticmethod
    def new(expt: Experiment):
        if not expt:
            raise TypeError('Experiment must not be None')
        expt_rep = ExperimentReport(expt)
        return expt_rep

    def __init__(self, expt: Experiment = None):
        TypeReport.__init__(self)
        Displayable.__init__(self)
        if not expt:
            raise TypeError('Subject must not be None or empty')
        self._label: str = expt.label
        self._assessor_count = len(expt.assessors)
        self._scan_count = 0
        self._process_resources(expt)
        self._process_shares(expt)
        for _, scan in expt.scans:
            self._process_modality(scan)
            self._scan_count += 1
        self._filter_modalities()
        if len(expt.custom_variables)> 0:
            self._has_custom_vars = True
            self._has_issues = True

    def __repr__(self):
        return f"{self.__class__.__name__}(Label={self._label!r}"

    def display(self, indent: str = "", recurse: bool = False):
        if not indent:
            indent = ""
        pad = indent+'* '
        output = f"{indent}{self.__class__.__name__}\n{pad}Label: {self._label}\n"
        if self._no_label > 0:
            output += f"{pad}NO LABEL Resources: {self._no_label}\n"
        if self._has_custom_vars:
            output += f"{pad}Custom Variables: YES\n"
        if len(self._shares) > 0:
            output += f"{pad}Shares: {', '.join(self._shares)}\n"
        if len(self._modalities) > 0:
            output += f"{pad}Non-Standard Modalities: {', '.join(self._modalities)}\n"
        output += f"{pad}Scans: {self._scan_count}"
        if self._assessor_count > 0:
            output += f"\n{pad}Assessors: {self._assessor_count}"
        print(output)

    @property
    def label(self) -> str:
        return self._label

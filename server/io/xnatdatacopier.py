####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

import html
import logging
import os
from pathlib import Path
import shutil
from typing import Dict

import xnat
from xnat.exceptions import XNATUploadError

from server import *
from .xnatscwriter import XnatServerConfigWriter


class XnatDataCopier:
    def __init__(self, source: xnat.session = None, target: xnat.session = None, logger: logging.Logger = None):
        if source is None:
            raise TypeError('Source must not be None')
        self._source = source
        if target is None:
            raise TypeError('Target must not be None')
        self._target = target
        if logger is None:
            logger = logging.getLogger('xpync.server.io.XnatDataCopier')
            handler = logging.StreamHandler()
            handler.setLevel(logging.WARNING)
            logger.addHandler(handler)
            handler.setFormatter(logging.Formatter("%(asctime)s %(name)-12s %(levelname)-8s %(message)s"))
        self._logger = logger

    def __repr__(self):
        return f"{self.__class__.__name__}(source={self._source!r}, target={self._target!r})"

    @property
    def source(self):
        return self._source

    @property
    def target(self):
        return self._target

    def copy(self, sc: ServerConfig, work_dir: Path):
        if not sc:
            raise TypeError('ServerConfig must not be None')
        if not work_dir.exists():
            raise ValueError(f"Path doesn't exist: {work_dir}")
        # Write the target metadata
        writer = XnatServerConfigWriter(self._target, logger=self._logger)
        writer.write_core(sc)
        # Process the projects
        all_proj_dir: Path = None
        try:
            all_proj_dir = self._ensure_dir_exists(work_dir / 'projects')
            for _, project in sc.projects:
                self._process_project(project, all_proj_dir)
        finally:
            self._clear_dir(all_proj_dir)

    def _clear_dir(self, path: Path):
        if path and path.exists():
            self._logger.debug(f"Removing directory: {path}")
            is_windows = os.name == 'nt'
            # Removing a non-empty tree often fails on Windows: https://bugs.python.org/issue33240
            shutil.rmtree(path, ignore_errors=is_windows)

    def _copy_fields(self, source, target):
        # Fields are where the custom vars are stored
        for field_id, value in source.fields.items():
            self._logger.debug(f"Copying field: {field_id} Value: {html.unescape(value)}")
            # Avoid double escaping of html chars
            target.fields[field_id] = html.unescape(value)

    def _create_rsrc_label_map(self, res_parent) -> Dict[str, str]:
        label_ids: Dict[str, str] = {}
        for rsrc_id, rsrc in res_parent.resources.items():
            label = rsrc.label if rsrc.label else 'NOLABEL'
            label_ids[label] = rsrc_id
        return label_ids

    def _ensure_dir_exists(self, path: Path) -> Path:
        if not path.exists():
            self._logger.debug(f"Creating directory: {path}")
            path.mkdir()
        return path

    def _get_extension(self, assessor: ExperimentAssessor) -> str:
        assessor_type = assessor.collection_type
        if (assessor_type == 'RTSTRUCT') or (assessor_type == 'SEG'):
            return 'dcm'
        elif assessor_type == 'AIM':
            return 'xml'
        else:
            return 'UNKNOWN'

    def _get_experiment_id(self, project: Project, subject: Subject, expt: Experiment):
        id_url = f"/data/projects/{project.id}/subjects/{subject.label}/experiments"
        expt_list = self._target.get_json(id_url)['ResultSet']['Result']
        for expt_data in expt_list:
            if expt_data['label'] == expt.label:
                return expt_data['ID']
        return None

    def _process_assessor(self, project: Project, subject: Subject, expt: Experiment, assessor: ExperimentAssessor,
                          all_assessor_dir: Path):
        assessor_dir: Path = None
        try:
            assessor_dir = self._ensure_dir_exists(all_assessor_dir / assessor.label)
            if assessor.xsi_type == 'icr:roiCollectionData':
                roi_type = assessor.collection_type
                if not roi_type:
                    raise ValueError("ROI collection type must not be None or empty")
                file = assessor_dir / f"{assessor.label}.{self._get_extension(assessor)}"
                source_url = f"/xapi/roi/projects/{project.id}/collections/{assessor.label}?tyoe={roi_type}"
                self._source.download(source_url, file)
                expt_id = self._get_experiment_id(project, subject, expt)
                target_url = (f"/xapi/roi/projects/{project.id}/sessions/{expt_id}/collections/{assessor.label}"
                              f"?type={roi_type}&overwrite=true")
                self._upload_roi(target_url, file)
            else:
                self._logger.warning(f"Assessor type {assessor.xsi_type} no supported yet")
        finally:
            self._clear_dir(assessor_dir)

    def _process_expt(self, project: Project, subject: Subject, expt: Experiment, all_expt_dir: Path):
        self._logger.info(f"Copy experiment {project.id}::{subject.label}::{expt.label}")
        expt_dir: Path = None
        try:
            source_expt = self._source.projects[project.id].subjects[subject.label].experiments[expt.label]
            self._copy_fields(source_expt,
                              self._target.projects[project.id].subjects[subject.label].experiments[expt.label])
            expt_dir = self._ensure_dir_exists(all_expt_dir / expt.label)
            self._process_expt_resources(project, subject, expt, expt_dir)

            # Copy the bulk data
            file = expt_dir / f"{expt.label}.zip"
            try:
                self._logger.debug(f"Copying {project.id}::{subject.label}::{expt.label} via {file}")
                source_expt.download(file)
                self._target.services.import_(str(file), project=project.id, subject=subject.label,
                                              experiment=expt.label, content_type='application/zip', overwrite='delete')
            except XNATUploadError:
                msg = f"Upload error for {project.id}::{subject.label}::{expt.label}: {file}"
                print(msg)
                self._logger.error(msg)
                return
            finally:
                try:
                    os.remove(file)
                except OSError:
                    pass

            for _, assessor in expt.assessors:
                self._process_assessor(project, subject, expt, assessor, expt_dir)
        finally:
            self._clear_dir(expt_dir)

    def _process_expt_resources(self, project: Project, subject: Subject, expt: Experiment, path: Path):
        res_path = self._process_res_dirs(expt, path)
        if not res_path:
            return
        target_map: Dict[str, str] = self._create_rsrc_label_map(
            self._target.projects[project.id].subjects[subject.label].experiments[expt.label])
        base_url = f"/data/projects/{project.id}/subjects/{subject.label}/experiments/{expt.label}/resources"
        desc = f"experiment: {project.id}::{subject.label}::{expt.label}"
        self._process_resources(expt, res_path, base_url, target_map, desc)

    def _process_project(self, project: Project, all_proj_dir: Path):
        self._logger.info(f"Copy project {project.id}")
        proj_dir: Path = None
        try:
            proj_dir = self._ensure_dir_exists(all_proj_dir / project.id)
            self._copy_fields(self._source.projects[project.id], self._target.projects[project.id])
            self._process_project_resources(project, proj_dir)
            # Process the subjects
            all_subj_dir = self._ensure_dir_exists(proj_dir / 'subjects')
            for _, subject in project.subjects:
                self._process_subject(project, subject, all_subj_dir)
        finally:
            self._clear_dir(proj_dir)

    def _process_project_resources(self, project: Project, path: Path):
        res_path = self._process_res_dirs(project, path)
        if not res_path:
            return
        target_map: Dict[str, str] = self._create_rsrc_label_map(self._target.projects[project.id])
        base_url = f"/data/projects/{project.id}/resources"
        desc = f"project: {project.id}"
        self._process_resources(project, res_path, base_url, target_map, desc)

    def _process_res_dirs(self, res_parent, path: Path) -> Path:
        if not res_parent.resource_dirs and not res_parent.resource_files:
            return None
        all_path = self._ensure_dir_exists(path / 'resources')
        for _, res_dir in res_parent.resource_dirs:
            self._ensure_dir_exists(all_path / res_dir.label)
        return all_path

    def _process_res_files(self, res_parent, base_url: str, path: Path):
        # Download files
        for _, source_file in res_parent.resource_files:
            collection = source_file.collection
            name = source_file.name
            if collection:
                file_path = path / collection / name
            else:
                file_path = path / name
            cat_id = source_file.category_id
            source_url = f"{base_url}/{cat_id}/files/{name}"
            self._source.download(source_url, file_path)

        rsrc_dir_list = self._source.get_json(base_url)['ResultSet']['Result']
        label_ids: Dict[str, str] = {}
        base_id = '-1'
        for rsrc_dir_data in rsrc_dir_list:
            label = rsrc_dir_data['label']
            if not label:
                base_id = rsrc_dir_data['xnat_abstractresource_id']
            else:
                label_ids[label] = rsrc_dir_data['xnat_abstractresource_id']
        for _, source_file in res_parent.resource_files:
            collection = source_file.collection
            name = source_file.name
            if collection:
                source_path = path / collection / name
                if collection not in label_ids:
                    self._logger.warning(f"No resource collection found: \"{collection}\". Skipping: {source_path}")
                    continue
                cat_id = label_ids[collection]
            else:
                source_path = path / name
                if base_id == '-1':
                    self._logger.warning(f"NO LABEL collection has no assigned ID: {base_url}. Skipping: {source_path}")
                    # Need to skip as actually doing a put with ID == -1 silently fails with a 200!
                    continue
                cat_id = base_id
            source_url = f"{base_url}/{cat_id}/files/{name}"
            # self._upload_file(url, source_path)

    def _process_resources(self, container, res_path: Path, base_url: str, target_map: Dict[str, str], desc: str):
        for _, source_file in container.resource_files:
            collection = source_file.collection
            name = source_file.name
            if collection:
                file_path = res_path / collection / name
            else:
                file_path = res_path / name
            source_cat_id = source_file.category_id
            source_url = f"{base_url}/{source_cat_id}/files/{name}"
            self._source.download(source_url, file_path)
            target_collection = collection if collection else 'NOLABEL'
            if target_collection not in target_map:
                self._logger.error(f"The {target_collection} resource collection has no ID in {desc}")
                continue
            target_cat_id = target_map[target_collection]
            target_url = f"{base_url}/{target_cat_id}/files/{name}"
            self._upload_file(target_url, file_path)

    def _process_subject(self, project: Project, subject: Subject, all_subj_dir: Path):
        self._logger.info(f"Copy subject {project.id}::{subject.label}")
        subj_dir: Path = None
        try:
            self._copy_fields(self._source.projects[project.id].subjects[subject.label],
                              self._target.projects[project.id].subjects[subject.label])
            subj_dir = self._ensure_dir_exists(all_subj_dir / subject.label)
            self._process_subject_resources(project, subject, subj_dir)
            # Process the subjects
            all_expt_dir = self._ensure_dir_exists(subj_dir / 'experiments')
            for _, expt in subject.experiments:
                self._process_expt(project, subject, expt, all_expt_dir)
        finally:
            self._clear_dir(subj_dir)

    def _process_subject_resources(self, project: Project, subject: Subject, path: Path):
        res_path = self._process_res_dirs(subject, path)
        if not res_path:
            return
        target_map: Dict[str, str] = self._create_rsrc_label_map(
            self._target.projects[project.id].subjects[subject.label])
        base_url = f"/data/projects/{project.id}/subjects/{subject.label}/resources"
        desc = f"subject: {project.id}::{subject.label}"
        self._process_resources(subject, res_path, base_url, target_map, desc)

    def _upload_file(self, url: str, path: Path):
        self._logger.debug(f"Uploading {path} to {url}")
        with open(path, 'rb') as file:
            self._target.upload(url, file)

    def _upload_roi(self, url: str, path: Path):
        stat = path.stat()
        if stat.st_size > 40*1 << 20:
            # 40MB limit for now
            self._logger.warning(f"File too large (>40MB) for upload: {path}")
            return
        self._logger.debug(f"Uploading {path} to {url}")
        with open(path, 'rb') as file:
            response = self._target.put(url, data=file, accepted_status=[200, 422])
            if response.status_code == 422:
                # Unprocessible entity - Non-fatal error from plugin
                self._logger.warning(f"Upload failed for {path.name} - {response.text}")

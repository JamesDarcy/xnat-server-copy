#!/usr/bin/env python

####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.
import json
import logging
from typing import Dict, Set

import xnat.exceptions

from server import *


class XnatServerConfigWriter:
    def __init__(self, connection: xnat.session = None, logger: logging.Logger = None):
        if connection is None:
            raise TypeError('XnatSession must not be None')
        self._conn = connection
        if logger is None:
            logger = logging.getLogger('xpync.server.io.XnatServerConfigWriter')
            handler = logging.StreamHandler()
            handler.setLevel(logging.WARNING)
            logger.addHandler(handler)
            handler.setFormatter(logging.Formatter("%(asctime)s %(name)-12s %(levelname)-8s %(message)s"))
        self._logger = logger

    def __repr__(self):
        return f"{self.__class__.__name__}(connection={self._conn!r})"

    @property
    def connection(self):
        return self._conn

    def write_core(self, sc: ServerConfig):
        # Creation pass
        self._create_users(sc)
        self._create_investigators(sc)
        self._create_projects(sc)

    def write_shares(self, sc: ServerConfig):
        # Configuration pass e.g. subjects shared to other projects
        self._config_projects(sc)

    def _config_assessors(self, project: Project, subject: Subject, expt: Experiment):
        # Iterate over assessors
        for _, assessor in expt.assessors:
            if assessor.xsi_type == 'icr:roiCollectionData':
                self._logger.debug((f"ROI collection detected in XnatServerConfigWriter::_config_assessors()"
                                    f"({project.id}::{subject.label}::{expt.label}::{assessor.label})"))
                continue
            self._config_assessor_sharing(project, subject, expt, assessor)

    def _config_assessor_sharing(self, project: Project, subject: Subject, expt: Experiment,
                                 assessor: ExperimentAssessor):
        for _, share in assessor.shares:
            url = (f"/data/projects/{project.id}/subjects/{subject.label}/experiments/{expt.label}"
                   f"/assessors/{assessor.label}/projects/{share.project}?label={share.label}")
            self._logger.debug(f"PUT {url}")
            self._conn.put(url)

    def _config_experiments(self, project: Project, subject: Subject):
        # Iterate over experiments
        for _, expt in subject.experiments:
            self._config_expt_sharing(project, subject, expt)
            self._config_assessors(project, subject, expt)

    def _config_expt_sharing(self, project: Project, subject: Subject, expt: Experiment):
        for _, share in expt.shares:
            url = (f"/data/projects/{project.id}/subjects/{subject.label}/experiments/{expt.label}"
                   f"/projects/{share.project}?label={share.label}")
            self._logger.debug(f"PUT {url}")
            self._conn.put(url)

    def _config_projects(self, sc: ServerConfig):
        # Iterate over projects
        for _, project in sc.projects:
            self._config_subjects(project)

    def _config_subjects(self, project: Project):
        # Iterate over subjects
        for _, subject in project.subjects:
            self._config_subject_sharing(subject)
            self._config_experiments(project, subject)

    def _config_subject_sharing(self, subject: Subject):
        # Iterate over shares
        for project, share in subject.shares:
            self._logger.info(f"Sharing subject \"{subject.label}\" to project \"{project}\"")
            url = (f"/data/projects/{subject.project}/subjects/{subject.label}/projects/{project}"
                   f"?label={share.label}")
            self._logger.debug(f"PUT {url}")
            self._conn.put(url)

    def _create_demographics_query(self, demogfx: Demographics):
        query: [str] = []
        for key, value in demogfx.items:
            query.append(f"{key}={value}")
        return '&'.join(query) if query else None

    def _create_experiments(self, project: Project, subject: Subject):
        # Iterate over experiments
        for label, expt in subject.experiments:
            self._logger.info(f"Creating experiment \"{label}\"")
            url = (f"/data/projects/{subject.project}/subjects/{subject.label}/experiments/{expt.label}"
                   f"?xsiType={expt.xsi_type}")
            if expt.uid:
                url += f"&UID={expt.uid}"
            if expt.date:
                url += f"&date={expt.date.strftime('%m/%d/%Y')}"
            self._logger.debug(f"PUT {url}")
            self._conn.put(url)
            self._create_expt_resource_dirs(project, subject, expt)
            self._create_expt_assessors(project, subject, expt)

    def _create_expt_assessors(self, project: Project, subject: Subject, expt: Experiment):
        for label, assessor in expt.assessors:
            if assessor.xsi_type == 'icr:roiCollectionData':
                # Skip as ROI collection will be created by plugin on data upload
                continue
            url = (f"/data/projects/{project.id}/subjects/{subject.label}/experiments/{expt.label}/assessors/{label}"
                   f"?xsiType={assessor.xsi_type}")
            self._logger.debug(f"PUT {url}")
            self._conn.put(url)

    def _create_expt_resource_dirs(self, project: Project, subject: Subject, expt: Experiment):
        base_url = f"/data/projects/{project.id}/subjects/{subject.label}/experiments/{expt.label}/resources"
        self._create_resource_dirs(subject, base_url)

    def _create_investigator_query(self, project: Project, sc: ServerConfig):
        query: [str] = []
        srv_invs: Dict[str, Investigator] = {}
        for inv_data in self._conn.get_json('/xapi/investigators'):
            inv = Investigator.new(inv_data)
            srv_invs[inv.get_key()] = inv
        if project.pi_firstname and project.pi_lastname:
            pi_key = project.pi_lastname+project.pi_firstname
            if pi_key and pi_key in srv_invs:
                proj_pi = srv_invs[pi_key]
                query.append(f"xnat:projectData/pi_xnat_investigatordata_id={proj_pi.id}")
                self._logger.info(f"Project {project.id}, set PI: {project.pi_firstname} {project.pi_lastname}")
        prefix = 'xnat:projectData/investigators/investigator'
        suffix = '/xnat_investigatordata_id='
        idx = 1
        for sc_key, sc_inv in sc.investigators:
            if (project.id in sc_inv.projects) and (sc_key in srv_invs):
                query.append(f"{prefix}[{idx}]{suffix}{srv_invs[sc_key].id}")
                idx += 1
                self._logger.info(f"Project {project.id}, add investigator: {sc_inv.firstname} {sc_inv.lastname}")
        return '&'.join(query) if query else None

    def _create_investigators(self, sc: ServerConfig):
        # Find pre-existing investigators
        preexist: Dict[str, Investigator] = {}
        inv_list = self._conn.get_json('/xapi/investigators')
        for inv_data in inv_list:
            pre_inv = Investigator.new(inv_data)
            preexist[pre_inv.get_key()] = pre_inv
        # Iterate over investigators
        for key, inv in sc.investigators:
            if key not in preexist:
                self._logger.info(f"Skipping investigator: {inv.firstname} {inv.lastname}")
                continue
            self._logger.info(f"Adding investigator: {inv.firstname} {inv.lastname}")
            url = '/xapi/investigators'
            data = json.loads(inv.json_str())
            self._logger.debug(f"POST {url} json={data}")
            self._conn.post(url, json=data)

    def _create_project(self, sc: ServerConfig, project: Project) -> bool:
        # Hack because XNATpy throws if an accepted response starts with <!DOCTYPE or <html>
        skip_response_content_check = self._conn.skip_response_content_check
        self._conn.skip_response_content_check = True
        # Create project
        self._logger.info(f"Creating project \"{project.title}\" with ID: {project.id}")
        data = json.loads(project.json_str(xnat=True))
        url = f"/data/projects/{project.id}"
        query = self._create_investigator_query(project, sc)
        if query:
            url += f"?{query}"
        self._logger.debug(f"PUT {url} json={data}")
        response = self._conn.put(url, data=data, accepted_status=[200, 403])
        # Restore prior status
        self._conn.skip_response_content_check = skip_response_content_check
        # Check response
        if response.status_code == 403:
            self._logger.warning(f"Error creating project. {project.id} has been used previously")
            return False
        self._logger.info(f"Setting project \"{project.id}\" accessibility: {project.accessibility}")
        url = f"/data/projects/{project.id}/accessibility/{project.accessibility}"
        self._logger.debug(f"PUT {url}")
        self._conn.put(url)
        url = f"/data/projects/{project.id}/prearchive_code/{project.prearchive_code}"
        self._logger.debug(f"PUT {url}")
        self._conn.put(url)
        url = f"/data/projects/{project.id}/quarantine_code/{project.quarantine_code}"
        self._logger.debug(f"PUT {url}")
        self._conn.put(url)
        return True

    def _create_projects(self, sc: ServerConfig):
        # First pass is project creation, adding users, subject/experiment/etc creation
        for project_id, project in sc.projects:
            if not self._create_project(sc, project):
                continue
            self._create_project_users(project)
            self._create_project_resource_dirs(project)
            self._create_subjects(project)

    def _create_project_resource_dirs(self, project: Project):
        base_url = f"/data/projects/{project.id}/resources"
        self._create_resource_dirs(project, base_url)

    def _create_project_users(self, project: Project):
        # Iterate over users
        for username, user in project.users:
            self._logger.info(f"Adding user \"{username}\" to project group: {user.group_id}")
            url = f"/data/projects/{project.id}/users/{user.displayname}/{username}"
            self._logger.debug(f"PUT {url}")
            self._conn.put(url)

    def _create_resource_dirs(self, source, base_url: str):
        existing: Set[str] = set()
        existing_list = self._conn.get_json(base_url)['ResultSet']['Result']
        for rsrc_dir_data in existing_list:
            if not rsrc_dir_data['label']:
                continue
            existing.add(rsrc_dir_data['label'])
        # Iterate over dirs
        for label, rsrc_dir in source.resource_dirs:
            if label in existing:
                self._logger.debug(f"Resource dir exists: {label}")
                continue
            self._logger.debug(f"Adding resource dir \"{label}\"")
            url = f"{base_url}/{label}"
            self._logger.debug(f"PUT {url}")
            self._conn.put(url)

    def _create_subject(self, project: Project, subject: Subject):
        self._logger.info(f"Creating subject \"{subject.label}\"")
        url = f"/data/projects/{project.id}/subjects/{subject.label}"
        if subject.demographics:
            url += f"?{self._create_demographics_query(subject.demographics)}"
        self._logger.debug(f"PUT {url}")
        self._conn.put(url)

    def _create_subject_resource_dirs(self, project: Project, subject: Subject):
        base_url = f"/data/projects/{project.id}/subjects/{subject.label}/resources"
        self._create_resource_dirs(subject, base_url)

    def _create_subjects(self, project: Project):
        # Iterate over subjects
        for _, subject in project.subjects:
            self._create_subject(project, subject)
            self._create_subject_resource_dirs(project, subject)
            self._create_experiments(project, subject)

    def _create_scans(self, project: Project, subject: Subject, expt: Experiment):
        for scan_id, scan in expt.scans:
            url = (f"/data/projects/{project.id}/subjects/{subject.label}/experiments/{expt.label}/scans/{scan_id}"
                   f"?xsiType={scan.xsi_type}")
            if scan.uid:
                url += f"&UID={scan.uid}"
            self._logger.debug(f"PUT {url}")
            self._conn.put(url)

    def _create_users(self, sc: ServerConfig):
        for username, sc_user in sc.users:
            if (username == "admin") or (username == self._conn.logged_in_user):
                self._logger.info(f"Skipping user: {username}")
                continue
            self._push_user(username, sc_user)

    def _push_user(self, username: str, sc_user: User):
        response = self._conn.get(f"/xapi/users/{username}", format='json', accepted_status=[200, 404])
        if response.ok:
            self._logger.info(f"Updating user: {username}")
            response = self._conn.put(f"/xapi/users/{username}", json=json.loads(sc_user.json_str()),
                                      accepted_status=[200, 304])
            if response.status_code == 304:
                # NotNodified
                self._logger.info(f"User {username} not modified")
        else:
            # NotFound
            self._logger.info(f"Creating user: {username}")
            self._conn.post('/xapi/users/', json=json.loads(sc_user.json_str()))
        roles = sc_user.roles
        if len(roles) > 0:
            self._conn.put(f"/xapi/users/{username}/roles", json=roles)
            self._logger.info(f"User {username} roles updated: {roles}")

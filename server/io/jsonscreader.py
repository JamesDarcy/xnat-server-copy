#!/usr/bin/env python

####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

import json
from pathlib import Path
from typing import Set

from server import *


class JsonServerConfigReader:
    def __init__(self, path: Path, project: str = None, no_cv: bool = False, no_assessors: bool = False, roi_only: bool = False):
        if path is None:
            raise TypeError('File must not be None')
        self._path = path
        self._no_cv = no_cv
        self._no_assessors = no_assessors
        self._roi_only = roi_only
        self._projectId: str = project if project else None
        self._project_shares: Set[str] = set()

    def __repr__(self):
        return f"{self.__class__.__name__}(path={self._path!r})"

    @property
    def path(self):
        return self._path

    def read(self) -> ServerConfig:
        sc = ServerConfig()
        with self._path.open('r') as file:
            json_data = json.load(file)
        self._read_users(json_data, sc)
        self._read_investigators(json_data, sc)
        self._read_projects(json_data, sc)
        return sc

    def _read_assessor_sharing(self, json_data: dict, assessor: ExperimentAssessor):
        if 'shares' not in json_data:
            return
        for sv in json_data['shares'].values():
            share = Share.new(sv)
            assessor.add_share(share)
            if self._projectId:
                self._project_shares.add(share.project)

    def _read_demographics(self, json_data: dict, subject: Subject):
        if 'demographics' in json_data:
            subject.demographics = Demographics.new_item(json_data['demographics'])

    def _read_experiment_assessors(self, json_data: dict, expt: Experiment):
        if 'assessors' not in json_data:
            return
        if self._no_assessors:
            return
        for av in json_data['assessors'].values():
            assessor = ExperimentAssessor.new(av)
            if self._roi_only and (assessor.xsi_type != 'icr:roiCollectionData'):
                continue
            expt.add_assessor(assessor)
            self._read_assessor_sharing(av, assessor)

    def _read_experiment_customvars(self, json_data: dict, expt: Experiment):
        if 'custom_variables' not in json_data:
            return
        if self._no_cv:
            return
        for cv_data in json_data['custom_variables'].values():
            cv = CustomVariable.new(cv_data)
            expt.add_custom_variable(cv)

    def _read_experiment_resources(self, json_data: dict, expt: Experiment):
        if 'resource_dirs' in json_data:
            for dv in json_data['resource_dirs'].values():
                rsrc_dir = ResourceDir.new(dv)
                expt.add_resource_dir(rsrc_dir)
        if 'resource_files' in json_data:
            for fv in json_data['resource_files'].values():
                rsrc_file = ResourceFile.new(fv)
                expt.add_resource_file(rsrc_file)

    def _read_experiment_sharing(self, json_data: dict, expt: Experiment):
        if 'shares' not in json_data:
            return
        for sv in json_data['shares'].values():
            share = Share.new(sv)
            expt.add_share(share)
            if self._projectId:
                self._project_shares.add(share.project)

    def _read_experiments(self, json_data: dict, subject: Subject):
        if 'experiments' not in json_data:
            return
        for ev in json_data['experiments'].values():
            expt = Experiment.new(ev)
            subject.add_experiment(expt)
            self._read_experiment_sharing(ev, expt)
            self._read_experiment_assessors(ev, expt)
            self._read_experiment_resources(ev, expt)
            self._read_experiment_customvars(ev, expt)
            self._read_scans(ev, expt)

    def _read_investigators(self, json_data: dict, sc: ServerConfig):
        if 'investigators' not in json_data:
            return
        for value in json_data['investigators'].values():
            sc.add_investigator(Investigator.new(value))

    def _read_project(self, sc: ServerConfig, project: Project, project_json: str, recurse: bool = True):
        sc.add_project(project)
        self._read_project_users(project_json, project)
        self._read_project_resources(project_json, project)
        # Recurse through subjects (default) to get full heirarchy instead of just top level project
        if recurse:
            self._read_subjects(project_json, project)

    def _read_projects(self, json_data: dict, sc: ServerConfig):
        if 'projects' not in json_data:
            return
        proj_list = json_data['projects'].values()
        for pv in proj_list:
            project = Project.new(pv)
            if self._projectId and (project.id != self._projectId):
                # Single project mode and project doesn't match
                continue
            self._read_project(sc, project, pv)
        if self._projectId and (len(self._project_shares) > 0):
            self._read_project_shares(sc, proj_list)

    def _read_project_resources(self, json_data: dict, project: Project):
        if 'resource_dirs' in json_data:
            for dv in json_data['resource_dirs'].values():
                rsrc_dir = ResourceDir.new(dv)
                project.add_resource_dir(rsrc_dir)
        if 'resource_files' in json_data:
            for fv in json_data['resource_files'].values():
                rsrc_file = ResourceFile.new(fv)
                project.add_resource_file(rsrc_file)

    def _read_project_shares(self, sc: ServerConfig, proj_list):
        self._logger.debug('Reading project shares')
        for proj_data in proj_list:
            project = Project.new(proj_data)
            if project.id in self._project_shares:
                # Get top level project data only
                self._read_project(sc, project, recurse=False)

    def _read_project_users(self, json_data: dict, project: Project):
        if 'users' not in json_data:
            return
        for uv in json_data['users'].values():
            project.add_user(ProjectUser.new(uv))

    def _read_scans(self, json_data: dict, expt: Experiment):
        if 'scans' not in json_data:
            return
        for sv in json_data['scans'].values():
            scan = Scan.new(sv)
            expt.add_scan(scan)

    def _read_subject_customvars(self, json_data: dict, subject: Subject):
        if 'custom_variables' not in json_data:
            return
        if self._no_cv:
            return
        for cv_data in json_data['custom_variables'].values():
            cv = CustomVariable.new(cv_data)
            subject.add_custom_variable(cv)

    def _read_subject_resources(self, json_data: dict, subject: Subject):
        if 'resource_dirs' in json_data:
            for dv in json_data['resource_dirs'].values():
                rsrc_dir = ResourceDir.new(dv)
                subject.add_resource_dir(rsrc_dir)
        if 'resource_files' in json_data:
            for fv in json_data['resource_files'].values():
                rsrc_file = ResourceFile.new(fv)
                subject.add_resource_file(rsrc_file)

    def _read_subject_sharing(self, json_data: dict, subject: Subject):
        if 'shares' not in json_data:
            return
        for sv in json_data['shares'].values():
            share = Share.new(sv)
            subject.add_share(share)
            if self._projectId:
                self._project_shares.add(share.project)

    def _read_subjects(self, json_data: dict, project: Project):
        if 'subjects' not in json_data:
            return
        for sv in json_data['subjects'].values():
            subject = Subject.new(sv)
            project.add_subject(subject)
            self._read_demographics(sv, subject)
            self._read_subject_sharing(sv, subject)
            self._read_subject_resources(sv, subject)
            self._read_subject_customvars(sv, subject)
            self._read_experiments(sv, subject)

    def _read_users(self, json_data: dict, sc: ServerConfig):
        if 'users' not in json_data:
            return
        for uv in json_data['users'].values():
            user = User.new(uv)
            sc.add_user(user)
            roles = uv.get('roles', None)
            if roles is not None:
                user.add_roles(roles)

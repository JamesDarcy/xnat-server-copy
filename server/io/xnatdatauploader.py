#!/usr/bin/env python

####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.
import logging
from pathlib import Path
from typing import Dict

import xnat
from xnat.exceptions import XNATUploadError

from server import *


class XnatDataUploader:
    def __init__(self, connection: xnat.session = None, logger: logging.Logger = None):
        if connection is None:
            raise TypeError('XnatSession must not be None')
        self._conn = connection
        if logger is None:
            logger = logging.getLogger('xpync.server.io.XnatDataUploader')
            handler = logging.StreamHandler()
            handler.setLevel(logging.WARNING)
            logger.addHandler(handler)
            handler.setFormatter(logging.Formatter("%(asctime)s %(name)-12s %(levelname)-8s %(message)s"))
        self._logger = logger

    def __repr__(self):
        return f"{self.__class__.__name__}(connection={self._conn!r})"

    @property
    def connection(self):
        return self._conn

    def upload(self, sc: ServerConfig, path: Path):
        if not path.exists():
            raise ValueError(f"Path doesn't exist: {path}")
        for _, project in sc.projects:
            self._process_project(project, path)

    def _get_experiment_id(self, project: Project, subject: Subject, expt: Experiment):
        id_url = f"/data/projects/{project.id}/subjects/{subject.label}/experiments"
        expt_list = self._conn.get_json(id_url)['ResultSet']['Result']
        for expt_data in expt_list:
            if expt_data['label'] == expt.label:
                return expt_data['ID']
        return None

    def _process_assessor(self, project: Project, subject: Subject, expt: Experiment, assessor: ExperimentAssessor,
                          path: Path):
        assess_path = path / 'assessors' / assessor.label
        if not assess_path.exists():
            raise IOError(f"Assessor path not found: {assess_path}")
        if assessor.xsi_type == 'icr:roiCollectionData':
            roi_type = assessor.collection_type
            if not roi_type:
                raise ValueError("ROI collection type must not be None or empty")
            file = assess_path / f"{assessor.label}.dcm"
            if not file.exists():
                raise IOError(f"Assessor not found: {file}")
            expt_id = self._get_experiment_id(project, subject, expt)
            url = (f"/xapi/roi/projects/{project.id}/sessions/{expt_id}/collections/{assessor.label}?type={roi_type}"
                   f"&overwrite=true")
            self._upload_roi(url, file)
        else:
            self._logger.warning(f"Assessor type {assessor.xsi_type} no supported yet")

    def _process_expt(self, project: Project, subject: Subject, expt: Experiment, path: Path):
        expt_path = path / 'experiments' / expt.label
        if not expt_path.exists():
            raise IOError(f"Experiment directory not found: {expt_path}")
        res_path = self._process_res_dirs(expt, expt_path)
        if res_path:
            res_url = f"/data/projects/{project.id}/subjects/{subject.label}/experiments/{expt.label}/resources"
            self._process_res_files(expt, res_url, res_path)
        file = expt_path / f"{expt.label}.zip"
        if not file.exists():
            msg = f"Experiment scan data file not found: {file}"
            self._logger.error(msg)
            return
        try:
            self._logger.debug(f"Uploading to {project.id}::{subject.label}::{expt.label}: {file}")
            self._conn.services.import_(str(file), project=project.id, subject=subject.label, experiment=expt.label,
                                        content_type='application/zip', overwrite='delete')
        except XNATUploadError as ex:
            msg = f"Upload error for {project.id}::{subject.label}::{expt.label}: {file}"
            self._logger.error(msg)
            return
        for _, assessor in expt.assessors:
            self._process_assessor(project, subject, expt, assessor, expt_path)

    def _process_project(self, project: Project, path: Path):
        project_path = path / 'projects' / project.id
        if not project_path.exists():
            raise IOError(f"Project directory not found: {project_path}")
        res_path = self._process_res_dirs(project, project_path)
        if res_path:
            res_url = f"/data/projects/{project.id}/resources"
            self._process_res_files(project, res_url, res_path)
        for _, subject in project.subjects:
            self._process_subject(project, subject, project_path)

    def _process_res_dirs(self, source, path: Path) -> Path:
        if not source.resource_dirs and not source.resource_files:
            return None
        all_path = path / 'resources'
        if not all_path.exists():
            raise IOError(f"Resource directory not found: {all_path}")
        for _, res_dir in source.resource_dirs:
            res_path = all_path / res_dir.label
            if not res_path.exists():
                raise IOError(f"Resource directory not found: {res_path}")
        return all_path

    def _process_res_files(self, source, base_url: str, path: Path):
        rsrc_dir_list = self._conn.get_json(base_url)['ResultSet']['Result']
        label_ids: Dict[str, str] = {}
        base_id = '-1'
        for rsrc_dir_data in rsrc_dir_list:
            label = rsrc_dir_data['label']
            if not label:
                base_id = rsrc_dir_data['xnat_abstractresource_id']
            else:
                label_ids[label] = rsrc_dir_data['xnat_abstractresource_id']
        for _, res_file in source.resource_files:
            collection = res_file.collection
            name = res_file.name
            if collection:
                source_path = path / collection / name
                if collection not in label_ids:
                    self._logger.warning(f"No collection found: \"{collection}\". Skipping: {source_path}")
                    continue
                cat_id = label_ids[collection]
            else:
                source_path = path / name
                if base_id == '-1':
                    self._logger.warning(f"NO LABEL collection has no assigned ID: {base_url}. Skipping: {source_path}")
                    # Need to skip as actually doing a put with ID == -1 silently fails with a 200!
                    continue
                cat_id = base_id
            url = f"{base_url}/{cat_id}/files/{name}"
            self._upload_file(url, source_path)

    def _process_scan(self, project: Project, subject: Subject, expt: Experiment, scan: Scan, path: Path):
        scan_path = path / 'scans' / f"{scan.id}.zip"
        if not scan_path.exists():
            raise IOError(f"Scan file not found: {scan_path}")
        self._logger.debug(f"Uploading to {project.id}::{subject.label}::{expt.label}: {scan_path}")
        self._conn.services.import_(str(scan_path), project=project.id, subject=subject.label, experiment=expt.label,
                                    content_type='application/zip', overwrite='append')

    def _process_subject(self, project: Project, subject: Subject, path: Path):
        subject_path = path / 'subjects' / subject.label
        if not subject_path.exists():
            raise IOError(f"Subject directory not found: {subject_path}")
        res_path = self._process_res_dirs(subject, subject_path)
        if res_path:
            res_url = f"/data/projects/{project.id}/subjects/{subject.label}/resources"
            self._process_res_files(subject, res_url, res_path)
        for _, expt in subject.experiments:
            self._process_expt(project, subject, expt, subject_path)

    def _upload_file(self, url: str, path: Path):
        self._logger.debug(f"Uploading {path} to {url}")
        with open(path, 'rb') as file:
            self._conn.upload(url, file)

    def _upload_roi(self, url: str, path: Path):
        stat = path.stat()
        if stat.st_size > 40*1 << 20:
            # 40MB limit for now
            self._logger.warning(f"File too large (>40MB) for upload: {path}")
            return
        self._logger.debug(f"Uploading {path} to {url}")
        with open(path, 'rb') as file:
            response = self._conn.put(url, data=file, accepted_status=[200, 422])
            if response.status_code == 422:
                # Unprocessible entity - Non-fatal error from plugin
                self._logger.warning(f"Upload failed for {path.name} - {response.text}")

####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

import html
import logging
from typing import Dict

import xnat


class XnatDataVerifier:
    def __init__(self, source: xnat.session = None, target: xnat.session = None, logger: logging.Logger = None):
        if source is None:
            raise TypeError('Source must not be None')
        self._source = source
        if target is None:
            raise TypeError('Target must not be None')
        self._target = target
        if logger is None:
            logger = logging.getLogger('xpync.server.io.XnatVerifier')
            handler = logging.StreamHandler()
            handler.setLevel(logging.WARNING)
            logger.addHandler(handler)
            handler.setFormatter(logging.Formatter("%(asctime)s %(name)-12s %(levelname)-8s %(message)s"))
        self._logger = logger

    def __repr__(self):
        return f"{self.__class__.__name__}(source={self._source!r}, target={self._target!r})"

    @property
    def source(self):
        return self._source

    @property
    def target(self):
        return self._target

    def verify(self) -> bool:
        source_ids = sorted(self._source.projects.data.keys())
        self._info('Verification start')
        verified = True
        # Check source projects all present in target
        for project_id in source_ids:
            if project_id not in self._target.projects:
                self._warning(f"Project {project_id} not found in target XNAT")
                verified = False
        if not verified:
            self._logger.info('Verification failed!')
            return verified
        # Check projects in depth
        for project_id in source_ids:
            verified &= self.verify_project(project_id)
        self._info(f"Verification {'complete' if verified else 'failed!'}")
        return verified

    def verify_project(self, project_id: str = None) -> bool:
        if project_id is None:
            raise TypeError('Project ID must not be None')
        self._info(f"Verifying project: {project_id}")
        verified = True
        if (not self._project_exists(project_id, self._source, 'source')) or \
                (not self._project_exists(project_id, self._target, 'target')):
            return False
        source_project = self._source.projects[project_id]
        target_project = self._source.projects[project_id]
        verified &= self._verify_project_meta(source_project, target_project)
        verified &= self._verify_resources(source_project.resources, target_project.resources, project_id)
        verified &= self._verify_subjects(source_project.subjects, target_project.subjects, project_id)
        self._info(f"Verification of project {project_id} {'complete' if verified else 'failed!'}")
        return verified

    def _copy_fields(self, source, target):
        # Fields are where the custom vars are stored
        for field_id, value in source.fields.items():
            self._logger.debug(f"Copying field: {field_id} Value: {html.unescape(value)}")
            # Avoid double escaping of html chars
            target.fields[field_id] = html.unescape(value)

    def _create_inv_dict(self, investigators) -> Dict[str, any]:
        inv_dict: Dict[str, any] = {}
        for _, inv in investigators.items():
            key = f"{inv.lastname}, {inv.firstname}"
            inv_dict[key] = inv
        return inv_dict

    def _debug(self, msg: str):
        if self._logger.isEnabledFor(logging.DEBUG):
            print(msg)
            self._logger.info(msg)

    def _info(self, msg: str):
        print(msg)
        self._logger.info(msg)

    def _project_exists(self, project_id: str, conn: xnat.session, desc: str = 'connected'):
        if project_id not in conn.projects:
            self._warning(f"Project {project_id} not found in {desc} XNAT")
            return False
        return True

    def _verify_assessor(self, source, target, parent_name: str = 'Parent') -> bool:
        verified = True
        verified &= self._verify_value(source.collection_type, target.collection_type, parent_name, 'collection_type')
        verified &= self._verify_value(source.uid, target.uid, parent_name, 'uid')
        verified &= self._verify_value(source.name, target.name, parent_name, 'name')
        verified &= self._verify_value(source.date, target.date, parent_name, 'date')
        verified &= self._verify_value(source.time, target.time, parent_name, 'time')
        desc = f"{parent_name}.files"
        exclude_keys = ['cat_ID', 'URI']
        for filename, source_file in source.files.items():
            if filename not in target.files:
                self._warning(f"File missing from {desc}: {filename}")
                verified = False
                continue
            verified &= self._verify_simple_dict(source_file.data, target.files[filename].data, parent_name,
                                                 exclude_keys)
        return verified

    def _verify_assessors(self, source, target, parent_name: str = 'Parent') -> bool:
        verified = True
        for _, source_assessor in source.items():
            label = source_assessor.label
            if label not in target:
                self._warning(f"Assessor missing from {parent_name}.assessors: {label}")
                verified = False
                continue
            verified &= self._verify_assessor(source_assessor, target[label], f"{parent_name}.assessors[{label}]")
        return verified

    def _verify_custom_vars(self, source, target, parent_name: str = 'Parent') -> bool:
        verified = True
        for key, value in source.items():
            if key not in target:
                self._warning(f"Custom variable missing from {parent_name}.fields: {key}")
                verified = False
                continue
            if value != target[key]:
                self._warning(f"Mismatch in custom variable {parent_name}.fields[{key}]:"
                              f" {value} != {target[key]}")
                verified = False
        return verified

    def _verify_demographics(self, source, target, parent_name: str = 'Parent') -> bool:
        verified = True
        desc = f"{parent_name}.demographics"
        verified &= self._verify_value(source.age, target.age, desc, 'age')
        verified &= self._verify_value(source.birth_weight, target.birth_weight, desc, 'birth_weight')
        verified &= self._verify_value(source.dob, target.dob, desc, 'dob')
        verified &= self._verify_value(source.education, target.education, desc, 'education')
        verified &= self._verify_value(source.education_desc, target.education_desc, desc, 'education_desc')
        verified &= self._verify_value(source.employment, target.employment, desc, 'employment')
        verified &= self._verify_value(source.ethnicity, target.ethnicity, desc, 'ethnicity')
        verified &= self._verify_value(source.gender, target.gender, desc, 'gender')
        verified &= self._verify_value(source.gestational_age, target.gestational_age, desc, 'gestational_age')
        verified &= self._verify_value(source.handedness, target.handedness, desc, 'handedness')
        verified &= self._verify_value(source.post_menstrual_age, target.post_menstrual_age, desc,
                                       'post_menstrual_age')
        verified &= self._verify_value(source.race, target.race, desc, 'race')
        verified &= self._verify_value(source.race2, target.race2, desc, 'race2')
        verified &= self._verify_value(source.race3, target.race3, desc, 'race3')
        verified &= self._verify_value(source.race4, target.race4, desc, 'race4')
        verified &= self._verify_value(source.race5, target.race5, desc, 'race5')
        verified &= self._verify_value(source.race6, target.race6, desc, 'race6')
        verified &= self._verify_value(source.ses, target.ses, desc, 'ses')
        verified &= self._verify_value(source.yob, target.yob, desc, 'yob')
        return verified

    def _verify_expt(self, source, target, project_id, subject_label) -> bool:
        self._info(f"Verifying experiment: {project_id}::{subject_label}::{source.label}")
        verified = True
        desc = f"{project_id}.subjects[{subject_label}].experiments[{source.label}]"
        verified &= self._verify_custom_vars(source.fields, target.fields, desc)
        verified &= self._verify_resources(source.resources, target.resources, desc)
        verified &= self._verify_sharing(source.sharing, target.sharing, desc)
        verified &= self._verify_assessors(source.assessors, target.assessors, desc)
        verified &= self._verify_scans(source.scans, target.scans, project_id, subject_label, source.label)
        self._info(f"Verification of experiment {project_id}::{subject_label}::{source.label} "
                   f"{'complete' if verified else 'failed!'}")
        return verified

    def _verify_expts(self, source_expts, target_expts, project_id, subject_label) -> bool:
        verified = True
        source_labels = sorted(source_expts.keys())
        for label in source_labels:
            if label not in target_expts:
                self._warning(f"Experiment missing from {project_id}.subjects[{subject_label}]: {label}")
                verified = False
                continue
            verified &= self._verify_expt(source_expts[label], target_expts[label], project_id, subject_label)
        return verified

    def _verify_files(self, source, target, parent_name: str = 'Parent') -> bool:
        verified = True
        exclude_keys = ['cat_ID', 'URI']
        for filename, source_file in source.items():
            if filename not in target:
                self._warning(f"File missing from {parent_name}: {filename}")
                verified = False
                continue
            # The "source_file.data" expression fetches the whole list of files for this listing. HUGE resource
            # drain for anything more than trivial file counts
            verified &= self._verify_simple_dict(source_file.data, target[filename].data, parent_name,
                                                 exclude_keys)
        return verified

    def _verify_investigator(self, source, target, parent_name: str = 'Parent') -> bool:
        verified = True
        verified &= self._verify_value(source.title, target.title, parent_name, 'title')
        verified &= self._verify_value(source.firstname, target.firstname, parent_name, 'firstname')
        verified &= self._verify_value(source.lastname, target.lastname, parent_name, 'lastname')
        verified &= self._verify_value(source.department, target.department, parent_name, 'department')
        verified &= self._verify_value(source.institution, target.institution, parent_name, 'institution')
        verified &= self._verify_value(source.email, target.email, parent_name, 'email')
        verified &= self._verify_value(source.phone, target.phone, parent_name, 'phone')
        return verified

    def _verify_investigators(self, source, target, parent_name: str = 'Parent') -> bool:
        verified = True
        # XNATPy uses only the lastname as a key, workaround this
        source_dict = self._create_inv_dict(source)
        target_dict = self._create_inv_dict(target)
        for key in sorted(source_dict.keys()):
            if key not in target_dict:
                self._warning(f"Investigator missing from {parent_name}.investigators: '{key}'a")
                verified = False
                continue
            desc = f"{parent_name}.investigators['{key}']"
            verified &= self._verify_investigator(source_dict[key], target_dict[key], desc)
        return verified

    def _verify_project_meta(self, source_project, target_project) -> bool:
        verified = True
        verified &= self._verify_simple_dict(source_project.data, target_project.data, source_project.id)
        verified &= self._verify_value(source_project.description, source_project.description, source_project.id,
                                       'description')
        verified &= self._verify_investigator(source_project.pi, target_project.pi, f"{source_project.id}.pi")
        verified &= self._verify_investigators(source_project.investigators, target_project.investigators,
                                               source_project.id)
        return verified

    def _verify_resource_catalog(self, source, target, parent_name: str = 'Parent', name: str = 'Name') -> bool:
        exclude_keys = ['xnat_abstractresource_id', 'element_name']
        if name == '':
            name = 'NO_LABEL'
        verified = self._verify_simple_dict(source.data, target.data, f"{parent_name}.resources[{name}]",
                                            exclude_keys)
        return verified

    def _verify_resource_files(self, source, target, parent_name: str = 'Parent',) -> bool:
        verified = True
        for key, source_file in source.items():
            if key not in target:
                self._warning(f"File missing from {parent_name}: {key}")
                verified = False
                continue
            target_file = target[key]
            # The "source_file.digest" expression fetches the whole list of files for this category ID. HUGE resource
            # drain for anything more than trivial file counts
            verified &= self._verify_value(source_file.digest, target_file.digest, f"{parent_name}.resources[{key}]",
                                           'digest')
        return verified

    def _verify_resources(self, source, target, parent_name: str = 'Parent') -> bool:
        verified = True
        for key, source_catalog in source.items():
            label = source_catalog.label
            if label not in target:
                self._warning(f"Catalog missing from {parent_name}.resources: {label if label else 'NO_LABEL'}")
                verified = False
                continue
            target_catalog = target[label]
            verified &= self._verify_resource_catalog(source_catalog, target_catalog, parent_name, label)
            verified &= self._verify_resource_files(source_catalog.files, target_catalog.files, parent_name)
        return verified

    def _verify_scan(self, source, target, project_id, subject_label, expt_label) -> bool:
        self._debug(f"Verifying scan: {project_id}::{subject_label}::{expt_label}::{source.id}")
        verified = True
        desc = f"{project_id}.subjects[{subject_label}].experiments[{expt_label}].scans[{source.id}]"
        # verified &= self._verify_files(source.files, target.files, f"{desc}.files")
        exclude_keys = ['image_session_ID', 'xnat_imageScanData_id', 'xnat_imagescandata_id']
        verified &= self._verify_simple_dict(source.data, target.data, desc, exclude_keys)
        return verified

    def _verify_scans(self, source_scans, target_scans, project_id, subject_label, expt_label) -> bool:
        verified = True
        source_ids = sorted(source_scans.keys())
        for scan_id in source_ids:
            if scan_id not in target_scans:
                self._warning(f"Scan missing from {project_id}.subjects[{subject_label}].scans[{expt_label}]:"
                              f" {scan_id}")
                verified = False
                continue
            verified &= self._verify_scan(source_scans[scan_id], target_scans[scan_id], project_id, subject_label,
                                          expt_label)
        return verified

    def _verify_sharing(self, source, target, parent_name: str = 'Parent') -> bool:
        verified = True
        for key, source_share in sorted(source.items()):
            if key not in target:
                self._warning(f"Share missing from {parent_name}.sharing: {key}")
                verified = False
                continue
            desc = f"{parent_name}.sharing[{key}]"
            verified &= self._verify_value(source_share.label, target[key].label, desc, 'label')
        return verified

    def _verify_simple_dict(self, source: dict, target: dict, parent_name: str = 'Parent', exclude_keys: list = None)\
            -> bool:
        if exclude_keys is None:
            exclude_keys = []
        verified = True
        for key, value in source.items():
            if key in exclude_keys:
                continue
            entry_ok = value == target[key]
            if not entry_ok:
                self._warning(f"Mismatch in {parent_name} - {key}: {value} != {target[key]}")
            verified &= entry_ok
        return verified

    def _verify_value(self, source, target, parent_name: str = 'Parent', name: str = 'Name') -> bool:
        verified = source == target
        if not verified:
            self._warning(f"Mismatch in {parent_name}.{name}: {source} != {target}")
        return verified

    def _verify_subject(self, source, target, project_id) -> bool:
        self._info(f"Verifying subject: {project_id}::{source.label}")
        verified = True
        desc = f"{project_id}.subjects[{source.label}]"
        verified &= self._verify_demographics(source.demographics, target.demographics, desc)
        verified &= self._verify_custom_vars(source.fields, target.fields, desc)
        verified &= self._verify_resources(source.resources, target.resources, desc)
        verified &= self._verify_sharing(source.sharing, target.sharing, desc)
        verified &= self._verify_expts(source.experiments, target.experiments, project_id, source.label)
        self._info(f"Verification of subject {project_id}::{source.label} "
                   f"{'complete' if verified else 'failed!'}")
        return verified

    def _verify_subjects(self, source_subjects, target_subjects, project_id) -> bool:
        verified = True
        source_labels = sorted(source_subjects.keys())
        for label in source_labels:
            if label not in target_subjects:
                self._warning(f"Subject missing from {project_id}.subjects: {label}")
                verified = False
                continue
            verified &= self._verify_subject(source_subjects[label], target_subjects[label], project_id)
        return verified

    def _warning(self, msg: str):
        print(msg)
        self._logger.warning(msg)

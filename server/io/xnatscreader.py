#!/usr/bin/env python

####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.
import logging
from typing import Set

import xnat

from server import *


class XnatServerConfigReader:
    def __init__(self, connection: xnat.session = None, logger: logging.Logger = None, project: str = None,
                 no_cv: bool = False, no_assessors: bool = False, roi_only: bool = False):
        if connection is None:
            raise TypeError('XnatSession must not be None')
        self._conn = connection
        if logger is None:
            logger = logging.getLogger('xpync.server.io.XnatServerConfigReader')
            handler = logging.StreamHandler()
            handler.setLevel(logging.WARNING)
            logger.addHandler(handler)
            handler.setFormatter(logging.Formatter("%(asctime)s %(name)-12s %(levelname)-8s %(message)s"))
        self._logger: logging.Logger = logger
        self._projectId: str = project if project else None
        self._project_shares: Set[str] = set()
        self._no_cv = no_cv
        self._no_assessors = no_assessors
        self._roi_only = roi_only

    def __repr__(self):
        return f"{self.__class__.__name__}(connection={self._conn!r})"

    @property
    def connection(self):
        return self._conn

    def read(self) -> ServerConfig:
        sc = ServerConfig()
        self._read_users(sc)
        self._read_investigators(sc)
        self._read_projects(sc)
        return sc

    def _find_field(self, fields, field_name):
        for field in fields:
            if field['field'] == field_name:
                return field
        else:
            return None

    def _read_assessor_sharing(self, project: Project, subject: Subject, expt: Experiment, assessor: ExperimentAssessor,
                               data: dict):
        if data is None:
            return
        self._logger.debug(f"Reading sharing for {project.id}::{subject.label}::{expt.label}::{assessor.label}")
        for item in data['items']:
            share = Share.new_item(item)
            assessor.add_share(share)
            if self._projectId:
                self._project_shares.add(share.project)

    def _read_demographics(self, project: Project, subject: Subject, data: dict):
        if data is None:
            return
        self._logger.debug(f"Reading demographics for {project.id}::{subject.label}")
        subject.demographics = Demographics.new_item(data['items'][0]['data_fields'])

    def _read_experiment_assessors(self, project: Project, subject: Subject, expt: Experiment, data: dict):
        if data is None:
            return
        if self._no_assessors:
            return
        self._logger.debug(f"Reading assessors for {project.id}::{subject.label}::{expt.label}")
        for item in data['items']:
            assessor = ExperimentAssessor.new_item(item)
            if self._roi_only and (assessor.xsi_type != 'icr:roiCollectionData'):
                continue
            expt.add_assessor(assessor)
            fields = item['children']
            self._read_assessor_sharing(project, subject, expt, assessor, self._find_field(fields, 'sharing/share'))

    def _read_experiment_custom_vars(self, project: Project, subject: Subject, expt: Experiment, data: dict):
        if data is None:
            return
        if self._no_cv:
            return
        self._logger.debug(f"Reading custom variables for {project.id}::{subject.label}::{expt.label}")
        for item in data['items']:
            cv = CustomVariable.new_item(item)
            expt.add_custom_variable(cv)

    def _read_experiment_resources(self, project: Project, subject: Subject, expt: Experiment):
        self._logger.debug(f"Reading resources for {project.id}::{subject.label}::{expt.label}")
        url = f"/data/projects/{project.id}/subjects/{subject.label}/experiments/{expt.label}/resources"
        raw = self._conn.get_json(url)
        rsrc_dir_list = raw['ResultSet']['Result']
        for rsrc_dir_data in rsrc_dir_list:
            if not rsrc_dir_data['label']:
                continue
            expt.add_resource_dir(ResourceDir.new(rsrc_dir_data))
        url = f"/data/projects/{project.id}/subjects/{subject.label}/experiments/{expt.label}/files"
        raw = self._conn.get_json(url)
        rsrc_list = raw['ResultSet']['Result']
        for rsrc_data in rsrc_list:
            expt.add_resource_file(ResourceFile.new(rsrc_data))

    def _read_experiment_sharing(self, project: Project, subject: Subject, expt: Experiment, data: dict):
        if data is None:
            return
        self._logger.debug(f"Reading sharing for {project.id}::{subject.label}::{expt.label}")
        for item in data['items']:
            share = Share.new_item(item)
            expt.add_share(share)
            if self._projectId:
                self._project_shares.add(share.project)

    def _read_experiments(self, project: Project, subject: Subject, data: dict):
        if data is None:
            return
        self._logger.debug(f"Reading experiments for {project.id}::{subject.label}")
        for item in data['items']:
            expt = Experiment.new_item(item)
            subject.add_experiment(expt)
            self._logger.info(f"Experiment: {project.id}::{subject.label}::{expt.label}")
            fields = item['children']
            self._read_experiment_sharing(project, subject, expt, self._find_field(fields, 'sharing/share'))
            self._read_experiment_assessors(project, subject, expt, self._find_field(fields, 'assessors/assessor'))
            self._read_experiment_resources(project, subject, expt)
            self._read_experiment_custom_vars(project, subject, expt, self._find_field(fields, 'fields/field'))
            self._read_scans(project, subject, expt, self._find_field(fields, 'scans/scan'))

    def _read_investigators(self, sc: ServerConfig):
        inv_list = self._conn.get_json('/xapi/investigators')
        for inv_data in inv_list:
            sc.add_investigator(Investigator.new(inv_data))

    def _read_project(self, sc: ServerConfig, project: Project, recurse: bool = True):
        response = self._conn.get(f"/data/projects/{project.id}/accessibility")
        project.accessibility = response.text
        response = self._conn.get(f"/data/projects/{project.id}/prearchive_code")
        try:
            project.prearchive_code = int(response.text)
        except ValueError:
            pass  # Deliberate no-op
        response = self._conn.get(f"/data/projects/{project.id}/quarantine_code")
        try:
            project.quarantine_code = int(response.text)
        except ValueError:
            pass  # Deliberate no-op
        self._logger.info(f"Project: {project.id}")
        sc.add_project(project)
        self._read_project_users(project)
        self._read_project_resources(project)
        # Recurse through subjects (default) to get full heirarchy instead of just top level project
        if recurse:
            self._read_subjects(project)

    def _read_projects(self, sc: ServerConfig):
        self._logger.debug('Reading projects')
        proj_list = self._conn.get_json('/data/projects')['ResultSet']['Result']
        for proj_data in proj_list:
            project = Project.new(proj_data)
            if self._projectId and (project.id != self._projectId):
                # Single project mode and project doesn't match
                continue
            self._read_project(sc, project)
        if self._projectId and (len(self._project_shares) > 0):
            self._read_project_shares(sc)

    def _read_project_resources(self, project: Project):
        self._logger.debug(f"Reading resources for {project.id}")
        rsrc_dir_list = self._conn.get_json(f"/data/projects/{project.id}/resources")['ResultSet']['Result']
        for rsrc_dir_data in rsrc_dir_list:
            if not rsrc_dir_data['label']:
                continue
            project.add_resource_dir(ResourceDir.new(rsrc_dir_data))
        rsrc_list = self._conn.get_json(f"/data/projects/{project.id}/files")['ResultSet']['Result']
        for rsrc_data in rsrc_list:
            project.add_resource_file(ResourceFile.new(rsrc_data))

    def _read_project_shares(self, sc: ServerConfig):
        self._logger.debug('Reading project shares')
        proj_list = self._conn.get_json('/data/projects')['ResultSet']['Result']
        for proj_data in proj_list:
            project = Project.new(proj_data)
            if project.id in self._project_shares:
                # Get top level project data only
                self._read_project(sc, project, recurse=False)

    def _read_project_users(self, project: Project):
        user_list = self._conn.get_json(f"/data/projects/{project.id}/users")['ResultSet']['Result']
        for user_data in user_list:
            project.add_user(ProjectUser.new(user_data))

    def _read_scans(self, project: Project, subject: Subject, expt: Experiment, data: dict):
        if data is None:
            return
        self._logger.debug(f"Reading scans for {project.id}::{subject.label}::{expt.label}")
        for item in data['items']:
            scan = Scan.new_item(item)
            expt.add_scan(scan)

    def _read_subject_custom_vars(self, project: Project, subject: Subject, data: dict):
        if data is None:
            return
        if self._no_cv:
            return
        self._logger.debug(f"Reading custom variables for {project.id}::{subject.label}")
        for item in data['items']:
            cv = CustomVariable.new_item(item)
            subject.add_custom_variable(cv)

    def _read_subject_resources(self, project: Project, subject: Subject):
        self._logger.debug(f"Reading resources for {project.id}::{subject.label}")
        raw = self._conn.get_json(f"/data/projects/{project.id}/subjects/{subject.label}/resources")
        rsrc_dir_list = raw['ResultSet']['Result']
        for rsrc_dir_data in rsrc_dir_list:
            if not rsrc_dir_data['label']:
                continue
            subject.add_resource_dir(ResourceDir.new(rsrc_dir_data))
        raw = self._conn.get_json(f"/data/projects/{project.id}/subjects/{subject.label}/files")
        rsrc_list = raw['ResultSet']['Result']
        for rsrc_data in rsrc_list:
            subject.add_resource_file(ResourceFile.new(rsrc_data))

    def _read_subject_sharing(self, project: Project, subject: Subject, data: dict):
        if data is None:
            return
        self._logger.debug(f"Reading sharing for {project.id}::{subject.label}")
        for item in data['items']:
            share = Share.new_item(item)
            subject.add_share(share)
            if self._projectId:
                self._project_shares.add(share.project)

    def _read_subjects(self, project: Project):
        self._logger.debug(f"Reading subjects for {project.id}")
        subj_list = self._conn.get_json(f"/data/projects/{project.id}/subjects")['ResultSet']['Result']
        for simple_subj in subj_list:
            subject = Subject.new(simple_subj)
            if subject.project != project.id:
                self._logger.debug(f"Subject {subject.label} is only shared to project {project.id}. Skipping")
                continue
            self._logger.info(f"Subject: {project.id}::{subject.label}")
            project.add_subject(subject)
            fields = self._conn.get_json(f"/data/projects/{project.id}/subjects/{subject.label}")
            fields = fields['items'][0]['children']
            self._read_demographics(project, subject, self._find_field(fields, 'demographics'))
            self._read_subject_sharing(project, subject, self._find_field(fields, 'sharing/share'))
            self._read_subject_resources(project, subject)
            self._read_subject_custom_vars(project, subject, self._find_field(fields, 'fields/field'))
            self._read_experiments(project, subject, self._find_field(fields, 'experiments/experiment'))

    def _read_users(self, sc: ServerConfig):
        user_list = self._conn.get_json('/xapi/users')
        for username in user_list:
            user_data = self._conn.get_json(f"/xapi/users/{username}")
            user = User.new(user_data)
            sc.add_user(user)
            role_data = self._conn.get_json(f"/xapi/users/{username}/roles")
            user.add_roles(role_data)

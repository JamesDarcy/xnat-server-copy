####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.
import logging
from pathlib import Path
from typing import Dict

import xnat.exceptions

from server import *


class XpXferMover:
    def __init__(self, source: xnat.session = None, target: xnat.session = None, logger: logging.Logger = None):
        if source is None:
            raise TypeError('XnatSession source must not be None')
        if target is None:
            raise TypeError('XnatSession target must not be None')
        self._source = source
        self._target = target
        if logger is None:
            logger = logging.getLogger('xpync.server.io.XpXferMover')
            handler = logging.StreamHandler()
            handler.setLevel(logging.WARNING)
            logger.addHandler(handler)
            handler.setFormatter(logging.Formatter("%(asctime)s %(name)-12s %(levelname)-8s %(message)s"))
        self._logger = logger

    def __repr__(self):
        return f"{self.__class__.__name__}(source={self._source!r}, target={self._target!r})"

    @property
    def source(self):
        return self._source

    def target(self):
        return self._target

####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.
import logging

import xnat.exceptions

from server import *


class XnatDeleter:
    def __init__(self, connection: xnat.session = None, logger: logging.Logger = None):
        if connection is None:
            raise TypeError('XnatSession must not be None')
        self._conn = connection
        if logger is None:
            logger = logging.getLogger('xpync.server.io.XnatDeleter')
            handler = logging.StreamHandler()
            handler.setLevel(logging.WARNING)
            logger.addHandler(handler)
            handler.setFormatter(logging.Formatter("%(asctime)s %(name)-12s %(levelname)-8s %(message)s"))
        self._logger = logger

    def __repr__(self):
        return f"{self.__class__.__name__}(connection={self._conn!r})"

    def clear_project(self, project_id: str):
        self._logger.info(f"Clearing project: {project_id}")
        self._clear_project_resources(project_id)
        self._clear_subjects(project_id)
        pass

    def clear_server(self):
        self._clear_project_contents()
        # self._clear_investigators()
        # self._disable_users()

    def delete_experiment(self, project_id: str, subject_label: str, expt_label: str, remove_files=True):
        self._clear_expt_resources(project_id, subject_label, expt_label)
        self._clear_assessors(project_id, subject_label, expt_label)
        self._clear_scans(project_id, subject_label, expt_label)
        self._logger.info(f"Deleting experiment: {expt_label}")
        url = f"/data/projects/{project_id}/subjects/{subject_label}/experiments/{expt_label}"
        query = {}
        if remove_files:
            query['removeFiles'] = 'true'
        self._logger.debug(f"DELETE {url}")
        self._conn.delete(url, query=query)

    def delete_subject(self, project_id: str, subject_label: str):
        self._clear_subject_resources(project_id, subject_label)
        self._clear_expts(project_id, subject_label)
        self._logger.info(f"Deleting subject: {subject_label}")
        url = f"/data/projects/{project_id}/subjects/{subject_label}"
        self._logger.debug(f"DELETE {url}")
        self._conn.delete(url)

    def _clear_assessors(self, project_id: str, subject_label: str, expt_label: str):
        base_url = f"/data/projects/{project_id}/subjects/{subject_label}/experiments/{expt_label}/assessors"
        response_json = self._conn.get_json(base_url)
        assessor_list = response_json['ResultSet']['Result']
        for assessor_data in assessor_list:
            assessor = ExperimentAssessor.new(assessor_data)
            if assessor.xsi_type == 'icr:roiCollectionData':
                # roi_type = assessor.collection_type
                # if not roi_type:
                #     raise ValueError("ROI collection type must not be None or empty")
                url = f"/xapi/roi/projects/{project_id}/collections/{assessor.label}"
                self._conn.delete(url)
            else:
                self._conn.delete(f"{base_url}/{assessor.label}")

    def _clear_expt_resources(self, project_id: str, subject_label: str, expt_label: str):
        self._logger.debug("Deleting experiment resources")
        base_url = f"/data/projects/{project_id}/subjects/{subject_label}/experiments/{expt_label}/resources"
        self._clear_resources(base_url)

    def _clear_expts(self, project_id: str, subject_label: str):
        response_json = self._conn.get_json(f"/data/projects/{project_id}/subjects/{subject_label}/experiments")
        expt_list = response_json['ResultSet']['Result']
        for expt_data in expt_list:
            expt = Experiment.new(expt_data)
            self.delete_experiment(project_id, subject_label, expt.label)

    def _clear_investigators(self):
        self._logger.info("Deleting investigators")
        inv_list = self._conn.get_json('/xapi/investigators')
        for inv_data in inv_list:
            investigator = Investigator.new(inv_data)
            self._logger.info(f"Deleting investigator: {investigator.id}")
            self._conn.delete(f"/xapi/investigators/{investigator.id}")

    def _clear_project_contents(self):
        self._logger.info("Deleting project contents")
        proj_list = self._conn.get_json('/data/projects')['ResultSet']['Result']
        for proj_data in proj_list:
            project = Project.new(proj_data)
            self.clear_project(project.id)

    def _clear_project_resources(self, project_id: str):
        self._logger.debug("Deleting project resources")
        base_url = f"/data/projects/{project_id}/resources"
        self._clear_resources(base_url)

    def _clear_resources(self, base_url: str):
        self._logger.debug("Deleting project resources")
        res_dir_list = self._conn.get_json(base_url)['ResultSet']['Result']
        # Ensure no label dir is deleted last
        base_id = '-1'
        for res_dir in res_dir_list:
            if res_dir['label']:
                url = f"{base_url}/{res_dir['xnat_abstractresource_id']}"
                self._logger.debug(f"DELETE {url}")
                self._conn.delete(url)
            else:
                base_id = res_dir['xnat_abstractresource_id']
        if base_id != '-1':
            url = f"{base_url}/{base_id}"
            self._logger.debug(f"DELETING NO LABEL")
            self._logger.debug(f"DELETE {url}")
            self._conn.delete(url)

    def _clear_scans(self, project_id: str, subject_label: str, expt_label: str):
        base_url = f"/data/projects/{project_id}/subjects/{subject_label}/experiments/{expt_label}/scans"
        response_json = self._conn.get_json(base_url)
        scan_list = response_json['ResultSet']['Result']
        for scan_data in scan_list:
            scan = Scan.new(scan_data)
            url = f"{base_url}/{scan.id}"
            self._logger.debug(f"DELETE {url}")
            self._conn.delete(url)

    def _clear_subject_resources(self, project_id: str, subject_label: str):
        self._logger.debug("Deleting subject resources")
        base_url = f"/data/projects/{project_id}/subjects/{subject_label}/resources"
        self._clear_resources(base_url)

    def _clear_subjects(self, project_id: str):
        subj_list = self._conn.get_json(f"/data/projects/{project_id}/subjects")['ResultSet']['Result']
        for subj_data in subj_list:
            subject = Subject.new(subj_data)
            self.delete_subject(project_id, subject.label)


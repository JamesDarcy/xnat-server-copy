#!/usr/bin/env python

####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.
import logging
from pathlib import Path

import xnat.exceptions

from server import *


class XnatDataDownloader:
    def __init__(self, connection: xnat.session = None, project: str = None, logger: logging.Logger = None,
                 use_cache: bool = False):
        if connection is None:
            raise TypeError('XnatSession must not be None')
        self._conn = connection
        self._projectId: str = project if project else None
        if logger is None:
            logger = logging.getLogger('xpync.server.io.XnatDataDownloader')
            handler = logging.StreamHandler()
            handler.setLevel(logging.WARNING)
            logger.addHandler(handler)
            handler.setFormatter(logging.Formatter("%(asctime)s %(name)-12s %(levelname)-8s %(message)s"))
        self._logger = logger
        self.use_cache: bool = use_cache

    def __repr__(self):
        return f"{self.__class__.__name__}(connection={self._conn!r})"

    @property
    def connection(self):
        return self._conn

    def download(self, sc: ServerConfig, path: Path):
        if not path.exists():
            path.mkdir()
        for _, project in sc.projects:
            if self._projectId and (project.id != self._projectId):
                # Single project mode and project doesn't match
                continue
            self._process_project(project, path)

    def _download_file(self, url, file):
        if not file.exists() or not self.use_cache:
            self._logger.debug(f"Downloading: {file}")
            self._conn.download(url, file)

    def _process_assessor(self, project: Project, subject: Subject, expt: Experiment, assessor: ExperimentAssessor,
                          path: Path):
        all_path = path / 'assessors'
        if not all_path.exists():
            self._logger.debug(f"Creating directory: {all_path}")
            all_path.mkdir()
        assess_path = all_path / assessor.label
        if not assess_path.exists():
            self._logger.debug(f"Creating assessor directory: {assess_path}")
            assess_path.mkdir()
        if assessor.xsi_type == 'icr:roiCollectionData':
            roi_type = assessor.collection_type
            if not roi_type:
                raise ValueError("ROI collection type must not be None or empty")
            url = f"/xapi/roi/projects/{project.id}/collections/{assessor.label}?tyoe={roi_type}"
            file = assess_path / f"{assessor.label}.dcm"
            self._download_file(url, file)
        else:
            self._logger.warning(f"Assessor type {assessor.xsi_type} no supported yet")

    def _process_expt(self, project: Project, subject: Subject, expt: Experiment, path: Path):
        all_path = path / 'experiments'
        if not all_path.exists():
            self._logger.debug(f"Creating directory: {all_path}")
            all_path.mkdir()
        expt_path = all_path / expt.label
        if not expt_path.exists():
            self._logger.debug(f"Creating experiment directory: {expt_path}")
            expt_path.mkdir()
        res_path = self._process_res_dirs(expt, expt_path)
        if res_path:
            self._process_expt_res_files(project, subject, expt, res_path)
        xExpt = self._conn.projects[project.id].subjects[subject.label].experiments[expt.label]
        file = expt_path / f"{expt.label}.zip"
        if not file.exists() or not self.use_cache:
            self._logger.debug(f"Downloading: {file}")
            xExpt.download(file)
        # for _, scan in expt.scans:
        #     self._process_scan(project, subject, expt, scan, expt_path)
        for _, assessor in expt.assessors:
            self._process_assessor(project, subject, expt, assessor, expt_path)

    def _process_expt_res_files(self, project: Project, subject: Subject, expt: Experiment, path: Path):
        base_url = f"/data/projects/{project.id}/subjects/{subject.label}/experiments/{expt.label}"
        self._process_res_files(expt, base_url, path)

    def _process_project(self, project: Project, path: Path):
        all_path = path / 'projects'
        if not all_path.exists():
            self._logger.debug(f"Creating directory: {all_path}")
            all_path.mkdir()
        project_path = all_path / project.id
        if not project_path.exists():
            self._logger.debug(f"Creating project directory: {project_path}")
            project_path.mkdir()
        res_path = self._process_res_dirs(project, project_path)
        if res_path:
            self._process_project_res_files(project, res_path)
        for _, subject in project.subjects:
            self._process_subject(project, subject, project_path)

    def _process_project_res_files(self, project: Project, path: Path):
        base_url = f"/data/projects/{project.id}/resources"
        self._process_res_files(project, base_url, path)

    def _process_res_dirs(self, source, path: Path) -> Path:
        if not source.resource_dirs and not source.resource_files:
            return None
        all_path = path / 'resources'
        if not all_path.exists():
            self._logger.debug(f"Creating directory: {all_path}")
            all_path.mkdir()
        for _, res_dir in source.resource_dirs:
            res_path = all_path / res_dir.label
            if not res_path.exists():
                self._logger.debug(f"Creating resource directory: {res_path}")
                res_path.mkdir()
        return all_path

    def _process_res_files(self, source, base_url: str, path: Path):
        for _, res_file in source.resource_files:
            collection = res_file.collection
            name = res_file.name
            if collection:
                target_path = path / collection / name
            else:
                target_path = path / name
            cat_id = res_file.category_id
            url = f"{base_url}/{cat_id}/files/{name}"
            self._download_file(url, target_path)

    def _process_scan(self, project: Project, subject: Subject, expt: Experiment, scan: Scan, path: Path):
        all_path = path / 'scans'
        if not all_path.exists():
            self._logger.debug(f"Creating directory: {all_path}")
            all_path.mkdir()
        xScan = self._conn.projects[project.id].subjects[subject.label].experiments[expt.label].scans[scan.id]
        file = all_path / f"{scan.id}.zip"
        if not file.exists() or not self.use_cache:
            self._logger.debug(f"Downloading: {file}")
            xScan.download(file)

    def _process_subject(self, project: Project, subject: Subject, path: Path):
        all_path = path / 'subjects'
        if not all_path.exists():
            self._logger.debug(f"Creating directory: {all_path}")
            all_path.mkdir()
        subject_path = all_path / subject.label
        if not subject_path.exists():
            self._logger.debug(f"Creating project directory: {subject_path}")
            subject_path.mkdir()
        res_path = self._process_res_dirs(subject, subject_path)
        if res_path:
            self._process_subject_res_files(project, subject, res_path)
        for _, expt in subject.experiments:
            self._process_expt(project, subject, expt, subject_path)

    def _process_subject_res_files(self, project: Project, subject: Subject, path: Path):
        base_url = f"/data/projects/{project.id}/subjects/{subject.label}"
        self._process_res_files(subject, base_url, path)

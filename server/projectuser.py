#!/usr/bin/env python

####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

from typing import Dict


class ProjectUser:
    @staticmethod
    def new(user: Dict[str, str]):
        return ProjectUser(user.get("login", None), user.get('GROUP_ID', None),
                           displayname=user.get("displayname", ""), firstname=user.get("firstname", ""),
                           lastname=user.get("lastname", ""), email=user.get("email", ""))

    def __init__(self, username: str, group_id: str = "", displayname: str = "", email: str = "", firstname: str = "",
                 lastname: str = ""):
        if (username is None) or (username == ""):
            raise TypeError("username must not be None or empty")
        if (group_id is None) or (group_id == ""):
            raise TypeError("group_id must not be None or empty")
        self._username = username
        self._group_id = group_id
        self.displayname = displayname if displayname is not None else ""
        self.firstname = firstname if firstname is not None else ""
        self.lastname = lastname if lastname is not None else ""
        self.email = email if email is not None else ""

    def __repr__(self):
        return (f"{self.__class__.__name__}(username={self._username!r}, group_id={self._group_id!r}, "
                f"displayname={self.displayname!r}, firstname={self.firstname!r}, "
                f"lastname={self.lastname!r}, email={self.email!r})")

    @property
    def group_id(self):
        return self._group_id

    def json_str(self, indent: str = "") -> str:
        if indent is None:
            indent = ""
        pad = indent+"  "
        out = f"{indent}{{\n"
        out += (f"{pad}\"login\": \"{self._username}\",\n"
                f"{pad}\"GROUP_ID\": \"{self._group_id}\",\n"
                f"{pad}\"displayname\": \"{self.displayname}\",\n"
                f"{pad}\"firstname\": \"{self.firstname}\",\n"
                f"{pad}\"lastname\": \"{self.lastname}\",\n"
                f"{pad}\"email\": \"{self.email}\"\n")
        out += f"{indent}}}"
        return out

    @property
    def username(self):
        return self._username

#!/usr/bin/env python

####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

from typing import Dict


class Scan:
    @staticmethod
    def new(scan: Dict[str, str]):
        xsi_type = scan.get('xsi:type', None)
        if not xsi_type:
            xsi_type = scan.get('xsiType', None)
        return Scan(scan.get('ID', None), xsi_type, scan.get('UID', ''))

    @staticmethod
    def new_item(item: Dict[str, dict]):
        meta: Dict[str, str] = item['meta']
        data: Dict[str, str] = item['data_fields']
        return Scan(data.get('ID', None), meta.get('xsi:type', None), data.get('UID', ''))

    def __init__(self, scan_id: str, xsi_type: str, uid: str = ''):
        if not scan_id:
            raise TypeError('ID must not be None or empty')
        if not xsi_type:
            raise TypeError('XsiType must not be None or empty')
        if not uid:
            uid = ''
        self._id: str = scan_id
        self._xsi_type: str = xsi_type
        self._uid = uid

    def __repr__(self):
        return f"{self.__class__.__name__}(id={self._id!r}, xsi_type={self._xsi_type!r}, uid={self._id!r}"

    @property
    def id(self):
        return self._id

    def json_str(self, indent: str = "", xnat: bool = False) -> str:
        if indent is None:
            indent = ""
        pad = indent+"  "
        out = f"{indent}{{\n"
        out += (f"{pad}\"ID\": \"{self._id}\",\n"
                f"{pad}\"xsi:type\": \"{self._xsi_type}\",\n"
                f"{pad}\"UID\": \"{self._uid}\"")
        out += f"\n{indent}}}"
        return out

    @property
    def uid(self):
        return self._uid

    @property
    def xsi_type(self):
        return self._xsi_type

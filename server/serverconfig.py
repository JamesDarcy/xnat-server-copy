#!/usr/bin/env python

####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

from typing import Dict

from .investigator import Investigator
from .project import Project
from .user import User


class ServerConfig:
    def __init__(self):
        self._investigators: Dict[str, Investigator] = {}
        self._projects: Dict[str, Project] = {}
        self._users: Dict[str, User] = {}

    def __repr__(self):
        return (f"{self.__class__.__name__}(projects={self._projects!r}, users={self._users!r}, "
                f"investigators={self._investigators!r})")

    def add_investigator(self, investigator: Investigator):
        if investigator is None:
            raise TypeError('Investigator must not be None')
        self._investigators[investigator.get_key()] = investigator

    def add_investigators(self, investigators: Dict[str, Investigator]):
        if investigators is None:
            raise TypeError('Investigator must not be None')
        for item in investigators.items():
            investigator = item[1]
            self._investigators[investigator.get_key()] = investigator

    def add_project(self, project: Project):
        if project is None:
            raise TypeError('Project must not be None')
        self._projects[project.id] = project

    def add_projects(self, projects: Dict[str, Project]):
        if projects is None:
            raise TypeError('Projects must not be None')
        for item in projects.items():
            project = item[1]
            self._projects[project.id] = project

    def add_user(self, user: User):
        if user is None:
            raise TypeError('User must not be None')
        self._users[user.username] = user

    def add_users(self, users: Dict[str, User]):
        if users is None:
            raise TypeError('Users must not be None')
        for item in users.items():
            user = item[1]
            self._users[user.username] = user

    @property
    def investigators(self):
        return sorted(self._investigators.items())

    def json_str(self, indent: str = "") -> str:
        if indent is None:
            indent = ""
        pad = indent+"  "
        dpad = pad+"  "
        separator = ",\n"
        out = f"{indent}{{\n"
        # Projects
        out = f"{out}{pad}\"projects\":\n{pad}{{\n"
        proj_list = []
        for _, project in self.projects:
            proj_list.append(f"{dpad}\"{project.id}\":\n{project.json_str(dpad, xnat=False)}")
        out = f"{out}{separator.join(proj_list)}\n"
        out = f"{out}{pad}}},\n"
        # Investigators
        out = f"{out}{pad}\"investigators\":\n{pad}{{\n"
        inv_list = []
        for _, inv in self.investigators:
            inv_list.append(f"{dpad}\"{inv.get_key()}\":\n{inv.json_str(dpad)}")
        out = f"{out}{separator.join(inv_list)}\n"
        out = f"{out}{pad}}},\n"
        # Users
        out = f"{out}{pad}\"users\":\n{pad}{{\n"
        user_list = []
        for _, user in self.users:
            user_list.append(f"{dpad}\"{user.username}\":\n{user.json_str(dpad)}")
        out = f"{out}{separator.join(user_list)}\n"
        out = f"{out}{pad}}}\n"
        # Finish
        out = f"{out}}}"
        return out

    @property
    def projects(self):
        return sorted(self._projects.items())

    @property
    def users(self):
        return sorted(self._users.items())

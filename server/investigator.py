#!/usr/bin/env python

####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

import json
from typing import Dict


class Investigator:
    @staticmethod
    def new(inv: Dict[str, str]):
        return Investigator(inv.get("firstname", None), inv.get("lastname", None),
                            inv.get("xnatInvestigatordataId", -1), email=inv.get("email", ""),
                            phone=inv.get("phone", ""), title=inv.get("title", ""),
                            department=inv.get("department", ""), institution=inv.get("institution", ""),
                            pi_projects=inv.get("primaryProjects", []), projects=inv.get("investigatorProjects", []))

    def __init__(self, firstname: str, lastname: str, investigator_id: int, email: str = "", phone: str = "",
                 title: str = "", department: str = "", institution: str = "", pi_projects: [str] = [],
                 projects: [str] = []):
        if (firstname is None) or (firstname == "") or (lastname is None) or (lastname == ""):
            raise TypeError("Firstname and lastname must not be null or empty")
        if investigator_id < 0:
            raise ValueError("Investigator ID must be positive")
        self._firstname: str = firstname
        self._lastname: str = lastname
        self._id: int = investigator_id
        self.email: str = email if email is not None else ""
        self.phone: str = phone if phone is not None else ""
        self.title: str = title if title is not None else ""
        self.department: str = department if department is not None else ""
        self.institution: str = institution if institution is not None else ""
        if pi_projects is not None:
            self._pi_projects: [str] = pi_projects.copy()
        else:
            self._pi_projects: [str] = []
        if projects is not None:
            self._projects: [str] = projects.copy()
        else:
            self._projects: [str] = []

    def __repr__(self):
        return (f"{self.__class__.__name__}(firstname={self._firstname!r}, lastname={self._lastname!r}, "
                f"id={self._id!r}, pi_projects={self._pi_projects!r}, projects={self._projects!r}, "
                f"email={self.email!r}, phone={self.phone!r}, title={self.title!r}, department={self.department!r}, "
                f"institution={self.institution!r})")

    @property
    def firstname(self) -> str:
        return self._firstname

    def get_key(self) -> str:
        return self._lastname+self._firstname

    @property
    def id(self) -> int:
        return self._id

    def json_str(self, indent: str = "") -> str:
        if indent is None:
            indent = ""
        pad = indent+"  "
        out = f"{indent}{{\n"
        out += (f"{pad}\"firstname\": \"{self._firstname}\",\n"
                f"{pad}\"lastname\": \"{self._lastname}\",\n"
                f"{pad}\"xnatInvestigatordataId\": {self._id},\n"
                f"{pad}\"primaryProjects\": {json.dumps(self.pi_projects)},\n"
                f"{pad}\"investigatorProjects\": {json.dumps(self.projects)},\n"
                f"{pad}\"email\": \"{self.email}\",\n"
                f"{pad}\"phone\": \"{self.phone}\",\n"
                f"{pad}\"title\": \"{self.title}\",\n"
                f"{pad}\"department\": \"{self.department}\",\n"
                f"{pad}\"institution\": \"{self.institution}\"\n")
        out += f"{indent}}}"
        return out

    @property
    def lastname(self) -> str:
        return self._lastname

    @property
    def pi_projects(self) -> [str]:
        return sorted(self._pi_projects.copy())

    @property
    def projects(self) -> [str]:
        return sorted(self._projects.copy())



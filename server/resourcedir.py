#!/usr/bin/env python

####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

from typing import Dict


class ResourceDir:
    @staticmethod
    def new(file: Dict[str, str]):
        return ResourceDir(file.get('label', None), content=file.get('content', ''),
                           format=file.get('format', ''), tags=file.get('tags', ''))

    # @staticmethod
    # def new_item(item: Dict[str, dict]):
    #     meta: Dict[str, str] = item['meta']
    #     data: Dict[str, str] = item['data_fields']
    #     return ResourceFile(data.get('label', None), meta.get('xsi:type', None))

    def __init__(self, label: str, content: str = '', format: str = '', tags: str = ''):
        if not label:
            raise TypeError('Label must not be None or empty')
        self._label: str = label
        self.content: str = content
        self.format: str = format
        self.tags: str = tags

    def __repr__(self):
        return (f"{self.__class__.__name__}(label={self._label!r}, content={self.content!r}, format={self.format!r},"
                f" tags={self.tags!r}")

    def json_str(self, indent: str = "", xnat: bool = False) -> str:
        if indent is None:
            indent = ""
        pad = indent+"  "
        out = f"{indent}{{\n"
        prop_list = [f"{pad}\"label\": \"{self._label}\""]
        if self.content:
            prop_list.append(f"{pad}\"content\": \"{self.content}\"")
        if self.format:
            prop_list.append(f"{pad}\"format\": \"{self.format}\"")
        if self.tags:
            prop_list.append(f"{pad}\"tags\": \"{self.tags}\"")
        separator = ',\n'
        out += separator.join(prop_list)
        out += f"\n{indent}}}"
        return out

    @property
    def label(self):
        return self._label

####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

from typing import Dict


class CustomVariable:
    @staticmethod
    def new(expt: Dict[str, str]):
        return CustomVariable(expt.get('name', None), expt.get('field', ''), expt.get('xsi:type', None))

    @staticmethod
    def new_item(item: Dict[str, dict]):
        meta: Dict[str, str] = item['meta']
        data: Dict[str, str] = item['data_fields']
        cvar = CustomVariable(data.get('name', None), data.get('field', None), meta.get('xsi:type', None))
        return cvar

    def __init__(self, name: str = None, field: str = None, xsi_type: str = None):
        if not name:
            raise TypeError('Name must not be None or empty')
        if not xsi_type:
            raise TypeError('XsiType must not be None or empty')
        if not field:
            field = ''
        self._name = name
        self._field = field
        self._xsi_type = xsi_type

    def __repr__(self):
        return (f"{self.__class__.__name__}(name={self._name!r}, field={self._field!r}, "
                f"xsi_type={self._xsi_type!r})")

    @property
    def field(self):
        return self._field

    @property
    def name(self):
        return self._name

    @property
    def xsi_type(self):
        return self._xsi_type

    def json_str(self, indent: str = "", xnat: bool = False) -> str:
        if indent is None:
            indent = ""
        pad = indent+"  "
        out = f"{indent}{{\n"
        out += (f"{pad}\"name\": \"{self._name}\",\n"
                f"{pad}\"field\": \"{self._field}\",\n"
                f"{pad}\"xsi:type\": \"{self._xsi_type}\"")
        out += f"\n{indent}}}"
        return out

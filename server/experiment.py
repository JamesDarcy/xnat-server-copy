####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

import datetime
from typing import Dict

from .customvar import CustomVariable
from .exptassessor import ExperimentAssessor
from .resourcedir import ResourceDir
from .resourcefile import ResourceFile
from .scan import Scan
from .share import Share


class Experiment:
    @staticmethod
    def new(expt: Dict[str, str]):
        xsi_type = expt.get('xsi:type', None)
        if not xsi_type:
            xsi_type = expt.get('xsiType', None)
        return Experiment(expt.get('label', None), xsi_type, expt.get('UID', ''), expt.get('date', ''))

    @staticmethod
    def new_item(item: Dict[str, dict]):
        meta: Dict[str, str] = item['meta']
        data: Dict[str, str] = item['data_fields']
        return Experiment(data.get('label', None), meta.get('xsi:type', None), data.get('UID', ''),
                          date=data.get('date', ''))

    def __init__(self, label: str, xsi_type: str, uid: str, date: str):
        if not label:
            raise TypeError('Label must not be None or empty')
        if not xsi_type:
            raise TypeError('XsiType must not be None or empty')
        if not uid:
            uid = ''
        self._label: str = label
        self._xsi_type: str = xsi_type
        self._uid: str = uid
        self._date: datetime.date = self._check_date(date)
        # Init remaining fields
        self._assessors: Dict[str, ExperimentAssessor] = {}
        self._customvars: Dict[str, CustomVariable] = {}
        self._rsrc_dirs: Dict[str, ResourceDir] = {}
        self._rsrc_files: Dict[str, ResourceFile] = {}
        self._scans: Dict[str, Scan] = {}
        self._shares: Dict[str, Share] = {}

    def __repr__(self):
        return (f"{self.__class__.__name__}(label={self._label!r}, xsi_type={self._xsi_type!r}, "
                f"uid={self._uid!r}, date={self._date!r})")

    def add_assessor(self, assessor: ExperimentAssessor):
        if assessor is None:
            raise TypeError("Assessor must not be None")
        self._assessors[assessor.label] = assessor

    def add_custom_variable(self, cv: CustomVariable):
        if cv is None:
            raise TypeError("Experiment must not be None")
        self._customvars[cv.name] = cv

    def add_resource_dir(self, rsrc_dir: ResourceDir):
        if rsrc_dir is None:
            raise TypeError("ResourceDir must not be None")
        self._rsrc_dirs[rsrc_dir.label] = rsrc_dir

    def add_resource_file(self, rsrc_file: ResourceFile):
        if rsrc_file is None:
            raise TypeError("ResourceDir must not be None")
        self._rsrc_files[rsrc_file.name] = rsrc_file

    def add_scan(self, scan: Scan):
        if scan is None:
            raise TypeError("Scan must not be None")
        self._scans[scan.id] = scan

    def add_share(self, share: Share):
        if share is None:
            raise TypeError("Share must not be None")
        self._shares[share.project] = share

    @property
    def assessors(self):
        return self._assessors.items()

    @property
    def custom_variables(self):
        return self._customvars.items()

    @property
    def date(self):
        return self._date

    def json_str(self, indent: str = "", xnat: bool = False) -> str:
        if indent is None:
            indent = ""
        pad = indent+"  "
        dpad = pad+"  "
        out = f"{indent}{{\n"
        out += (f"{pad}\"label\": \"{self._label}\",\n"
                f"{pad}\"xsi:type\": \"{self._xsi_type}\",\n"
                f"{pad}\"UID\": \"{self._uid}\"")
        if self._date:
            out += (',\n'
                    f"{pad}\"date\": \"{self._date.strftime('%Y-%m-%d')}\"")
        if xnat:
            out += '\n'
        else:
            separator = ',\n'
            # Scans
            out += f",\n{pad}\"scans\":\n{pad}{{"
            scan_list = []
            for scan_id, scan in self.scans:
                scan_list.append(f"{dpad}\"{scan_id}\":\n{scan.json_str(dpad)}")
            if scan_list:
                out += f"\n{separator.join(scan_list)}\n{pad}}}"
            else:
                out += f"}}"
            # Assessors
            if len(self._assessors) > 0:
                out += f",\n{pad}\"assessors\":\n{pad}{{"
                assessor_list = []
                for label, assessor in self.assessors:
                    assessor_list.append(f"{dpad}\"{label}\":\n{assessor.json_str(dpad)}")
                if assessor_list:
                    out += f"\n{separator.join(assessor_list)}\n{pad}}}"
                else:
                    out += f"}}"
            # Shares
            if len(self._shares) > 0:
                out += f",\n{pad}\"shares\":\n{pad}{{"
                share_list = []
                for project, share in self.shares:
                    share_list.append(f"{dpad}\"{project}\":\n{share.json_str(dpad)}")
                if share_list:
                    out += f"\n{separator.join(share_list)}\n{pad}}}"
                else:
                    out += f"}}"
            # ResourceDirs
            if len(self._rsrc_dirs) > 0:
                out += f",\n{pad}\"resource_dirs\":\n{pad}{{"
                dir_list = []
                for dir_label, rsrc_dir in self.resource_dirs:
                    dir_list.append(f"{dpad}\"{dir_label}\":\n{rsrc_dir.json_str(dpad)}")
                if dir_list:
                    out += f"\n{separator.join(dir_list)}\n{pad}}}"
                else:
                    out += f"}}"
            # ResourceFiles
            if len(self._rsrc_files) > 0:
                out += f",\n{pad}\"resource_files\":\n{pad}{{"
                file_list = []
                for filename, rsrc_file in self.resource_files:
                    file_list.append(f"{dpad}\"{filename}\":\n{rsrc_file.json_str(dpad)}")
                if file_list:
                    out += f"\n{separator.join(file_list)}\n{pad}}}"
                else:
                    out += f"}}"
            # CustomVars
            if len(self._customvars) > 0:
                out += f",\n{pad}\"custom_variables\":\n{pad}{{"
                cv_list = []
                for name, cv in self.custom_variables:
                    cv_list.append(f"{dpad}\"{name}\":\n{cv.json_str(dpad)}")
                if cv_list:
                    out += f"\n{separator.join(cv_list)}\n{pad}}}"
                else:
                    out += f"}}"
        out += f"\n{indent}}}"
        return out

    @property
    def label(self):
        return self._label

    @property
    def resource_dirs(self):
        return self._rsrc_dirs.items()

    @property
    def resource_files(self):
        return self._rsrc_files.items()

    @property
    def scans(self):
        return self._scans.items()

    @property
    def shares(self):
        return self._shares.items()

    @property
    def uid(self):
        return self._uid

    @property
    def xsi_type(self):
        return self._xsi_type

    def _check_date(self, date_str: str) -> datetime.date:
        if not date_str:
            return None
        date_format = '%Y-%m-%d'
        try:
            dt = datetime.datetime.strptime(date_str, date_format)
        except ValueError:
            return None
        return dt.date()

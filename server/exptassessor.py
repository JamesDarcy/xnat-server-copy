####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

from typing import Dict

from .share import Share


class ExperimentAssessor:
    @staticmethod
    def new(expt: Dict[str, str]):
        xsi_type = expt.get('xsi:type', None)
        if not xsi_type:
            xsi_type = expt.get('xsiType', None)
        return ExperimentAssessor(expt.get('label', None), xsi_type, collection_type=expt.get('collectionType', ''))

    @staticmethod
    def new_item(item: Dict[str, dict]):
        meta: Dict[str, str] = item['meta']
        data: Dict[str, str] = item['data_fields']
        return ExperimentAssessor(data.get('label', None), meta.get('xsi:type', None),
                                  collection_type=data.get('collectionType', ''))

    def __init__(self, label: str = None, xsi_type: str = None, collection_type: str = ''):
        if not label:
            raise TypeError('Label must not be None or empty')
        if not xsi_type:
            raise TypeError('XsiType must not be None or empty')
        if not collection_type:
            collection_type = ''
        self._label: str = label
        self._xsi_type: str = xsi_type
        self._collection_type = collection_type
        self._shares: Dict[str, Share] = {}

    def __repr__(self):
        return (f"{self.__class__.__name__}(label={self._label!r}, xsi_type={self._xsi_type!r}, "
                f"collection_type={self._collection_type!r}")

    def add_share(self, share: Share):
        if share is None:
            raise TypeError("Share must not be None")
        self._shares[share.project] = share

    @property
    def collection_type(self):
        return self._collection_type

    def json_str(self, indent: str = "", xnat: bool = False) -> str:
        if indent is None:
            indent = ""
        pad = indent+"  "
        dpad = pad+"  "
        out = f"{indent}{{\n"
        out += (f"{pad}\"label\": \"{self._label}\",\n"
                f"{pad}\"xsi:type\": \"{self._xsi_type}\"")
        if self._collection_type:
            out += f",\n{pad}\"collectionType\": \"{self._collection_type}\""
        if not xnat:
            if len(self._shares) > 0:
                separator = ',\n'
                # Shares
                out += f",\n{pad}\"shares\":\n{pad}{{"
                share_list = []
                for project, share in self.shares:
                    share_list.append(f"{dpad}\"{project}\":\n{share.json_str(dpad)}")
                if share_list:
                    out += f"\n{separator.join(share_list)}\n{pad}}}"
                else:
                    out += f"}}"
        out += f"\n{indent}}}"
        return out

    @property
    def label(self):
        return self._label

    @property
    def shares(self):
        return sorted(self._shares.items())

    @property
    def xsi_type(self):
        return self._xsi_type

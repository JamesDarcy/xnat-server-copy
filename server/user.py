#!/usr/bin/env python

####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

import json
from typing import Dict, Set


class User:
    @staticmethod
    def new(user: Dict[str, str]):
        return User(user.get("username", None), firstname=user.get("firstName", ""), lastname=user.get("lastName", ""),
                    email=user.get("email", ""), verified=user.get('verified', False),
                    enabled=user.get('enabled', False), secured=user.get('secured', True))

    def __init__(self, username: str, firstname: str = "", lastname: str = "", email: str = "", verified: bool = False,
                 enabled: bool = False, secured: bool = True):
        if (username is None) or (username == ""):
            raise TypeError("username must not be None or empty")
        self._username = username
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.verified = verified
        self.enabled = enabled
        self.secured = secured
        self._roles: Set[str] = set()

    def __repr__(self):
        return (f"{self.__class__.__name__}(username={self._username!r}, firstname={self.firstname!r}, "
                f"lastname={self.lastname!r}, email={self.email!r}, verified={self.verified!r}, "
                f"enabled={self.enabled!r}, secured={self.secured!r})")

    def add_role(self, role: str):
        self._roles.add(role)

    def add_roles(self, roles: [str]):
        for role in roles:
            self._roles.add(role)

    def json_str(self, indent: str = "", xnat: bool = False) -> str:
        if indent is None:
            indent = ""
        pad = indent+"  "
        out = f"{indent}{{\n"
        out += (f"{pad}\"username\": \"{self._username}\",\n"
                f"{pad}\"firstName\": \"{self.firstname}\",\n"
                f"{pad}\"lastName\": \"{self.lastname}\",\n"
                f"{pad}\"email\": \"{self.email}\",\n"
                f"{pad}\"verified\": {self._lower(self.verified)},\n"
                f"{pad}\"enabled\": {self._lower(self.enabled)},\n"
                f"{pad}\"secured\": {self._lower(self.secured)}")
        if xnat:
            out += "\n"
        else:
            out += f",\n{pad}\"roles\": {json.dumps(self.roles)}\n"
        out += f"{indent}}}"
        return out

    @property
    def roles(self):
        return sorted(list(self._roles))

    @property
    def username(self):
        return self._username

    def _lower(self, value: bool):
        return "true" if value else "false"

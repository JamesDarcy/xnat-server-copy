####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.

from typing import Dict

from .customvar import CustomVariable
from .demographics import Demographics
from .experiment import Experiment
from .resourcedir import ResourceDir
from .resourcefile import ResourceFile
from .share import Share


class Subject:
    @staticmethod
    def new(subject: Dict[str, str]):
        return Subject(subject.get("project", None), subject.get("label", None))

    def __init__(self, project: str, label: str):
        if not project:
            raise TypeError("Project must not be None or empty")
        if not label:
            raise TypeError("Label must not be None or empty")
        self._customvars: Dict[str, CustomVariable] = {}
        self._experiments: Dict[str, Experiment] = {}
        self._label: str = label
        self._project: str = project
        self._rsrc_dirs: Dict[str, ResourceDir] = {}
        self._rsrc_files: Dict[str, ResourceFile] = {}
        self._shares: Dict[str, Share] = {}
        self.demographics: Demographics = None

    def __repr__(self):
        return f"{self.__class__.__name__}(project={self._project!r}, label={self._label!r})"

    def add_custom_variable(self, cv: CustomVariable):
        if cv is None:
            raise TypeError("Experiment must not be None")
        self._customvars[cv.name] = cv

    def add_experiment(self, expt: Experiment):
        if expt is None:
            raise TypeError("Experiment must not be None")
        self._experiments[expt.label] = expt

    def add_resource_dir(self, rsrc_dir: ResourceDir):
        if rsrc_dir is None:
            raise TypeError("ResourceDir must not be None")
        self._rsrc_dirs[rsrc_dir.label] = rsrc_dir

    def add_resource_file(self, rsrc_file: ResourceFile):
        if rsrc_file is None:
            raise TypeError("ResourceDir must not be None")
        self._rsrc_files[rsrc_file.name] = rsrc_file

    def add_share(self, share: Share):
        if share is None:
            raise TypeError("Share must not be None")
        self._shares[share.project] = share

    @property
    def custom_variables(self):
        return sorted(self._customvars.items())

    @property
    def experiments(self):
        return sorted(self._experiments.items())

    def json_str(self, indent: str = "", xnat: bool = False) -> str:
        if indent is None:
            indent = ""
        pad = indent+"  "
        dpad = pad+"  "
        out = f"{indent}{{\n"
        out += (f"{pad}\"project\": \"{self._project}\",\n"
                f"{pad}\"label\": \"{self._label}\"")
        if xnat:
            out += "\n"
        else:
            separator = ",\n"
            # Experiments
            out += f",\n{pad}\"experiments\":\n{pad}{{"
            expt_list = []
            for label, expt in self.experiments:
                expt_list.append(f"{dpad}\"{label}\":\n{expt.json_str(dpad)}")
            if expt_list:
                out += f"\n{separator.join(expt_list)}\n{pad}}}"
            else:
                out += f"}}"
            # Shares
            if len(self._shares) > 0:
                out += f",\n{pad}\"shares\":\n{pad}{{"
                share_list = []
                for project, share in self.shares:
                    share_list.append(f"{dpad}\"{project}\":\n{share.json_str(dpad)}")
                if share_list:
                    out += f"\n{separator.join(share_list)}\n{pad}}}"
                else:
                    out += f"}}"
            # Demographics
            if self.demographics:
                out += f",\n{pad}\"demographics\":\n{self.demographics.json_str(pad)}"
            # ResourceDirs
            if len(self._rsrc_dirs) > 0:
                out += f",\n{pad}\"resource_dirs\":\n{pad}{{"
                dir_list = []
                for dir_label, rsrc_dir in self.resource_dirs:
                    dir_list.append(f"{dpad}\"{dir_label}\":\n{rsrc_dir.json_str(dpad)}")
                if dir_list:
                    out += f"\n{separator.join(dir_list)}\n{pad}}}"
                else:
                    out += f"}}"
            # ResourceFiles
            if len(self._rsrc_files) > 0:
                out += f",\n{pad}\"resource_files\":\n{pad}{{"
                file_list = []
                for filename, rsrc_file in self.resource_files:
                    file_list.append(f"{dpad}\"{filename}\":\n{rsrc_file.json_str(dpad)}")
                if file_list:
                    out += f"\n{separator.join(file_list)}\n{pad}}}"
                else:
                    out += f"}}"
            # CustomVars
            if len(self._customvars) > 0:
                out += f",\n{pad}\"custom_variables\":\n{pad}{{"
                cv_list = []
                for name, cv in self.custom_variables:
                    cv_list.append(f"{dpad}\"{name}\":\n{cv.json_str(dpad)}")
                if cv_list:
                    out += f"\n{separator.join(cv_list)}\n{pad}}}"
                else:
                    out += f"}}"
        out += f"\n{indent}}}"
        return out

    @property
    def label(self):
        return self._label

    @property
    def project(self):
        return self._project

    @property
    def resource_dirs(self):
        return sorted(self._rsrc_dirs.items())

    @property
    def resource_files(self):
        return sorted(self._rsrc_files.items())

    @property
    def shares(self):
        return sorted(self._shares.items())

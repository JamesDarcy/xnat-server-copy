####
# Author: James Darcy, Division of Radiotherapy and Imaging, Institute of Cancer Research, UK.


class XsiType:
    @staticmethod
    def modality(xsi_type: str) -> str:
        if not xsi_type:
            raise TypeError('XsiType must not be None or empty')
        start = xsi_type.index(':')+1
        if start < 0:
            raise ValueError(f"Invalid XsiType: {xsi_type}")
        end = start
        for c in xsi_type[start:]:
            if c.islower():
                end += 1
            else:
                break
        modality = xsi_type[start:end].upper()
        return modality
